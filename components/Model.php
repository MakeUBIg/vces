<?php

namespace app\components;
use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

abstract class Model extends ActiveRecord
{
     
	public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    public function getAll($fieldName,$condition=NULL)
    {
      $all =  $this->find()->where(['del_status' => '0']);
        if($condition)
        {
           $all = $all->andWhere($condition);  
        }
        $all = $all->all();
        $vals = [];
        foreach ($all as $val) {
            $vals[$val->id] = $val->$fieldName;
        }
        return $vals;  
    }

    public function getManyById($id,$compareField,$getField)
    {
        $all =  $this->find()->where([$compareField => $id])->all();
        $vals = [];
        foreach ($all as $val) {
            $vals[$val->id] = $val->$getField;
        }
        return $vals;
    }

    public function getAllByValue($field,$value)
    {

    }

    public function checkActive($attribute, $params, $validator)
    {
        if(!is_a(Yii::$app,'yii\console\Application'))
        {
            if(\Yii::$app->request->isAjax)
            {
                $model = new $this->modelClass();
                $record = $model::find()->where([$attribute => $this->$attribute,'del_status' => '0'])->all();
                if(count($record) > 0)
                {
                    $this->addError($attribute,ucfirst($attribute).' already Taken! Please Try A different '.ucfirst($attribute));
                    return false;
                }    
            }
            else
            {
                if(\Yii::$app->controller->action->id == 'create')
                {
                    $record = self::find()->where([$attribute => $this->$attribute,'del_status' => '0'])->all();
                    if(count($record) > 0)
                    {
                        $this->addError($attribute,ucfirst($attribute).' already Taken! Please Try A different '.ucfirst($attribute));
                        return false;
                    }    
                }
                elseif(\Yii::$app->controller->action->id == 'update')
                {
                    $id = 0;
                    $record = self::find()->where(['del_status' => '0',$attribute => $this->$attribute])->one();
                    if(count($record) > 0)
                    {
                        if($record->id != $this->id)
                        {
                            $this->addError($attribute,ucfirst($attribute).' already Taken! Please Try A different '.ucfirst($attribute));
                            return false;
                        }    
                    }
                }
            }
        }
    }
}
