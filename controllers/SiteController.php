<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Registration;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $contact = new \app\models\ContactMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contact->load($params))
        {
            $this->layout = false;
            if($contact->save(false))
            {   
                \Yii::$app->mailer->compose(['name' => $contact->name, 'email' => $contact->email, 'mobile' => $contact->mobile, 'message' => $contact->message, 'programm' => $contact->programm])
                    ->setFrom('admin@vces.ca')
                    ->setTo($contact->email)
                    ->setCc('vikkumar648@gmail.com')
                    ->setSubject('VCES Contact Info')
                    ->setBcc('praveen@makeubig.com','MakeUBIG ADMIN')
                    ->send();    
                return $this->redirect('success');
            }
            else
            {
                p($contact->getErrors());
            }
                    
        }
        return $this->render('contact',['contact' => $contact]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
     public function actionSuccess()
    {
        return $this->render('success');
    }
    public function actionOurTeam()
    {
        return $this->render('ourteam');
    }
    public function actionGraducate()
    {
        return $this->render('graducate');
    }
    public function actionUndergraducate()
    {
        return $this->render('undergraducate');
    }
    public function actionPartnerunivr()
    {
        return $this->render('partnerunivr');
    }
    public function actionFee()
    {
        return $this->render('fee');
    }
    public function actionImmegration()
    {
        return $this->render('immegration');
    }
    public function actionServicsupport()
    {
        return $this->render('servicsupport');
    }
    public function actionAddmissionprocess()
    {
        return $this->render('addmissionprocess');
    }
    public function actionRegistration()
    {
        $registration = new \app\models\Registration();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $registration->load($params))
        {
            $this->layout = false;
            if($registration->save(false))
            {   
                return $this->redirect('success');
            }
            else
            {
                p($registration->getErrors());
            }
                    
        }
        return $this->render('registration',['registration' => $registration]);
    }
    public function actionProcedure()
    {
        return $this->render('procedure');
    }
}
