<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_seo".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $keyword_text
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUserPage $page
 */
class PageSeo extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['keyword_text'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_id' => Yii::t('app', 'Page ID'),
            'keyword_text' => Yii::t('app', 'Keyword Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MubUserPage::className(), ['id' => 'page_id'])->where(['del_status' => '0']);
    }
}
