<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_mail".
 *
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $programm
 * @property string $message
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 */
class ContactMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'mobile', 'message'], 'required'],
            [['programm', 'message', 'del_status'], 'string'],
             ['email','email','message' => 'This is not a valid email format'],
             ['mobile','number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'email', 'mobile'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'programm' => 'Programm',
            'message' => 'Message',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}
