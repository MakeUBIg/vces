<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registration".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $birth_name
 * @property string $prefered_name
 * @property string $email
 * @property string $street_no
 * @property string $address_line1
 * @property string $address_line2
 * @property string $town
 * @property string $province
 * @property string $gender
 * @property string $postal_zip
 * @property integer $home_phone
 * @property integer $work_telephone
 * @property string $date_of_birth
 * @property string $country
 * @property string $first_choice
 * @property string $second_choice
 * @property string $undecide
 * @property string $registring_term
 * @property string $immediate_education
 * @property string $applying
 * @property string $first_launguage
 * @property string $launguage_thoughout
 * @property string $iltes_score
 * @property string $tofel_score
 * @property string $gmat_score
 * @property string $official_school_name
 * @property string $town_city
 * @property string $province_state
 * @property string $from
 * @property string $to
 * @property integer $year_of_graduation
 * @property string $i_agree
 */
class Registration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'birth_name', 'email', 'street_no', 'address_line1', 'address_line2', 'town', 'province', 'postal_zip', 'home_phone', 'date_of_birth', 'country', 'first_choice', 'second_choice', 'undecide', 'registring_term', 'immediate_education', 'applying', 'first_launguage', 'launguage_thoughout', 'iltes_score', 'tofel_score', 'gmat_score', 'official_school_name', 'town_city', 'province_state', 'from', 'to', 'year_of_graduation', 'i_agree'], 'required'],
            [['gender'], 'string'],
            ['email','email','message' => 'This is not a valid email format'],
             ['home_phone','number'],
             ['work_telephone','number'],
             ['street_no','number'],
             [['date_of_birth', 'to_date'], 'date'],
            [['home_phone', 'work_telephone', 'year_of_graduation'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'birth_name', 'prefered_name', 'email', 'street_no', 'address_line1', 'address_line2', 'town', 'province', 'postal_zip', 'date_of_birth', 'first_choice', 'second_choice', 'undecide', 'registring_term', 'immediate_education', 'applying', 'first_launguage', 'launguage_thoughout', 'iltes_score', 'tofel_score', 'gmat_score', 'official_school_name', 'town_city', 'province_state'], 'string', 'max' => 50],
            [['country', 'from', 'to', 'i_agree'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'birth_name' => 'Birth Name',
            'prefered_name' => 'Prefered Name',
            'email' => 'Email',
            'street_no' => 'Street No',
            'address_line1' => 'Address Line1',
            'address_line2' => 'Address Line2',
            'town' => 'Town',
            'province' => 'Province',
            'gender' => 'Gender',
            'postal_zip' => 'Postal Zip',
            'home_phone' => 'Home Phone',
            'work_telephone' => 'Work Telephone',
            'date_of_birth' => 'Date Of Birth',
            'country' => 'Country',
            'first_choice' => 'First Choice',
            'second_choice' => 'Second Choice',
            'undecide' => 'Undecide',
            'registring_term' => 'Registring Term',
            'immediate_education' => 'Immediate Education',
            'applying' => 'Applying',
            'first_launguage' => 'First Launguage',
            'launguage_thoughout' => 'Launguage Thoughout',
            'iltes_score' => 'Iltes Score',
            'tofel_score' => 'Tofel Score',
            'gmat_score' => 'Gmat Score',
            'official_school_name' => 'Official School Name',
            'town_city' => 'Town City',
            'province_state' => 'Province State',
            'from' => 'From',
            'to' => 'To',
            'year_of_graduation' => 'Year Of Graduation',
            'i_agree' => 'I Agree',
        ];
    }
}
