 function isMobileDevice()
{
	var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

    return isMobile;
}


// This is to fix navbar when not Being fixed Automatically

// $(window).scroll(function() {
//     var scrollVal = $(this).scrollTop();
//     if (scrollVal > 450) 
//     {
//         $('.navbar').css('display', 'block');
//         //if not mobile device
//         if(!isMobileDevice())
//         {
//         	//when the div position starts
//         	if(scrollVal > 1160)
// 	        {
// 	      		if($('#fixonscroll').offset().top() + $('#fixonscroll').height() 
//                                            >= $('.footer').offset().top - 10)
// 	      		{

//         			$("#fixonscroll").removeClass('fixed-div');
// 	      		}
//     			if($(document).scrollTop() + window.innerHeight < $('.footer').offset().top)
//     			{
//         			$("#fixonscroll").addClass('fixed-div');
//     			}
// 	    	}
// 	    	else
// 	    	{
// 	    		$("#fixonscroll").removeClass('fixed-div');
// 	    	}
//         }        
//     } 
//     else 
//     {
//         $('.navbar').css('display', 'block');
//     }
// });

addEventListener("load", function() {
 setTimeout(hideURLbar, 0);
	//Adding No-Ui Slider to the DOM
var price = document.getElementById('priceinfilter');
var rangeLow = document.getElementById('pricelow');
var rangeHigh = document.getElementById('pricehigh');
var priceUpLimit = document.getElementById('uplimit');
var priceLowLimit = document.getElementById('lowlimit');

if(rangeLow !== null)
	{
		noUiSlider.create(price, {
			start: [2000, 50000],
			connect: true,
			range: {
				'min': 2000,
				'max': 50000
			}
		});			

		price.noUiSlider.on('update', function( values, handle ) {
	        if ( handle ) {
	            rangeHigh.value = values[handle];
	            rangeHigh.innerHTML = values[handle];            
	        } 
	        else {
	            rangeLow.value = values[handle];
	            rangeLow.innerHTML = values[handle];
	        }
	    });
	    price.noUiSlider.on('set', function( values, handle ) {
	        if ( handle ) {
	           rangeHigh.value = values[handle];      
	           priceUpLimit.value =  values[handle];        
	        } 
	        else 
	        {
	           rangeLow.value = values[handle]; 
	           priceLowLimit.value =  values[handle];
	        }
	        submitSearch();
	    });
	}
});

// 	//Adding smartlook tracking on load

// 	window.smartlook||(function(d) {
//     var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
//     var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
//     c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
//     })(document);
//     smartlook('init', 'efd4d1aafc14574999e31a11717e204ef2f520a3');


// }, false);

// Uppercase word's first character
String.prototype.ucwords = function() {
  str = this.toLowerCase();
  return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
  	function(s){
  	  return s.toUpperCase();
	});
};
   

function submitSearch()
{   
	var form = $('#searchTab').serializeArray();
	var selected = $("#sort-search option:selected").val();
    //var selected = $('#displayview').val();
	form[form.length] = {name: "sort", value: selected};
	$.ajax({
         type:'GET',
         url:"/search/search-result",
         data : form,
        success: function(result)
        {   
        	$('#dynamic-div').html(result);
        	var resultcount = $('#result_count').val();
        	var cityname = $('#city_name').val();
        	$('#resultcount').text("("+resultcount+")");
        	$('#cityname').text(cityname);
            if($('#searchPlacesPg').length)
            {
            	var searchParam = $('#searchPlacesPg').val();
            }
            if($('#searchPlacesHouse').length)
            {
                var searchParam = $('#searchPlacesHouse').val();
            }
        	if(searchParam != '')
        	{
	        	$('#search-city-name').text(' in '+searchParam);
        	}
        	var roomsFor = $('#rooms_for').val();
        	if(roomsFor != "")
        	{
	        	$('#search-result-for').text('for '+ roomsFor.ucwords());
        	}
        	var roomsType = $('#rooms_type').val();
        	if(roomsType != "")
        	{
        		if(roomsType === 'pg')
        		{
        			$('#search-room-type').text('PG');	
        		}else
        		{
		        	$('#search-room-type').text(roomsType.ucwords());
        		}
        	}
        }
    });
}

$('.selectview').on('click',function(){
    var text = $(this).attr('id');
    $('#displayview').val(text);
    $('#searchTab').submit();
});

var getNumber = function(e)
{
	// Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return 'nan';
    }
    else
    {
    	// Ensure that it is a number and stop the keypress
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	        return 'nan';
	    }
	    else
	    {
	    	//this is where we get the number
	    	return this.value;
	    }

    }
    
};


//get the click evenyt on the data toggle modal
$("a[data-toggle=modal]").click(function(ev) {
    ev.preventDefault();
    var target = $(this).attr("href");
	// load the url and show modal on success
    $("#myModal .modal-content").load(target, function() { 
         $("#myModal").modal("show"); 
    });
});

function hideURLbar(){ 
	window.scrollTo(0,1); 
}

function cancelVisit(id){
	var conf = confirm('Are You sure you want to cancel this visit ? ');
	if(conf)
	{
		$.ajax({
         type:'POST',
         url:"/property/cancel-visit",
         data : {'scheduid':id},
        success: function(result)
        {
            console.log(JSON.stringify(result));
        	if(result=='1')
        	{
                setTimeout(function() {
                    location.reload();//reload page
                }, 4000);
        	}else
        	{
        		alert('There was a problem cancelling the scheduled visit');
        	}
        }
    	});
	}
}

$(window).scroll(function() {
    var scrollVal = $(this).scrollTop();
    if (scrollVal > 450) {
        $('#outernav').css('display', 'block');
    } else {
        $('#outernav').css('display', 'none');
    }
});

$('#searchFlat').on('click',function(){

    var roomtype = 'flat';
    if(roomtype === "")
    {
     roomtype = 'flat';   
    }
    $('#rooms_type').val(roomtype);

    var rooms = $('#room_for_house').val();
    $('#rooms_for').val(rooms);

    var location = $('#searchPlacesHouse').val();
    $('#searchPlaces').val(location);

    if(location === ""){
        $("#demo2").css('display','block');
    }else{
        $('#home-main-search').submit();
    }
});

$('#searchPg').on('click',function(){

    var roomtype = 'pg';
    $('#rooms_type').val(roomtype);

    var rooms = $('#room_for').val();
    $('#rooms_for').val(rooms);

    var location = $('#searchPlacesPg').val();
    $('#searchPlaces').val(location);


    if(location === ""){
        $("#demo").css('display','block');
    }else{
        $('#home-main-search').submit();
    }    
});

$('#searchTab input').on('click',function(e){
	var id = $(this).attr('id');
    var inputs = ['searchPlacesPg','searchPlacesHouse'];
 	if(jQuery.inArray(id,inputs) !== -1)
    {
		var searchVal = $('#'+id).val();
		if(searchVal !== ''){
			submitSearch();
		}
	}else{ 

		if($(this).is(':radio'))
		{
			var field = $(this).attr('name'); 
			var value = $(this).val();
			$('#'+field).val(value);
		}
		submitSearch();
	}
});


//$('#searchTab input').on('click',function(e){

$('#dates-field1').on('change', function() {
    submitSearch();
});


$('#dates-field2').on('change', function() {
    submitSearch();
});

$('#house_type').on('change', function() {
    var val = $('#house_type').val();
    $('#property_home').val(val);
});

$('#acc_search').on('change', function() {
    submitSearch();
});

$('#sort-search').on('change', function() {
    submitSearch();
});

$('#priceinfilter').on('click',function(){
	submitSearch();
});

$("#schedule_visits").on('beforeSubmit',function(e){
	e.preventDefault();
    var form = $('#schedule_visits').serialize();
		$.ajax({
         type:'POST',
         url:"/mub-admin/dashboard/save-enquiry",
         data:form,
         success: function(result)
	        {
	        	if(result)
	        	{
	        		 $('#schedule_visits').html('<h3 style="text-align:center; color:#fff;">Thank you For Scheduling a visit with Us.</h3> <br/><p style="text-align:center; padding-bottom:5.8em; color:#fff; font-size:16px;line-height:35px;">We have send you a mail for confirmation, a copy of which will be send to the owner,<br/> You can expect a call from the owner soon.</h4>');
	        		setTimeout(function() {
					      // Do something after 5 seconds
					      location.reload();//reload page
					}, 4000);
	        	}
	        	else
	        	{
	        		$('#loginmodal').click();
	        	}
	        }
    	});
        return false;
	});

$(document).on("beforeSubmit", "#list_homes", function(e){
     e.preventDefault();
     var form = $('#list_homes').serialize();
     $.ajax({
         type:'POST',
         url:"/site/registration",
         data:form,
         success: function(result)
	        {
	        	if(result)
	        	{
	        	$('#list_homes').html('<br/><p style="text-align:center; color:#4B829D; font-size:30px;line-height:35px;">Thank you for providing us your data.<br/> Our team will get in touch with you soon..</h4>');
	        	 setTimeout(function() {
                      // Do something after 5 seconds
                      location.reload();//reload page
                    }, 4000);
                }
	        	else
	        	{
	        	$('#loginmodal').click();
	        	}
	        }
    	});
     return false;
});

$(document).on("beforeSubmit", "#contact_office", function(e){
     e.preventDefault();
     var form = $('#contact_office').serialize();
     $.ajax({
         type:'POST',
         url:"/site/registration",
         data:form,
         success: function(result)
	        {
	        	if(result)
	        	{
	        	$('#contact_office').html('<br/><p style="text-align:center; color:#4B829D; font-size:16px;line-height:35px; margin-top:3em;">Thank you for providing us your data.<br/> Our team will get in touch with you soon..</h4>');
	        	 setTimeout(function() {
				      // Do something after 5 seconds
				      location.reload();//reload page
					}, 4000);
	        	}
	        	else
	        	{
	        	$('#loginmodal').click();
	        	}
	        }
    	});
     return false;
});
$(document).on("beforeSubmit", "#contact", function(e){
     e.preventDefault();
     var form = $('#contact').serialize();
     $.ajax({
         type:'POST',
         url:"/site/contact",
         data:form,
         success: function(result)
            {
                if(result)
                {
                $('#contact').html('<br/><p style="text-align:center; color:#4B829D; font-size:16px;line-height:35px; margin-top:3em;">Thank you for providing us your data.<br/> Our team will get in touch with you soon..</h4>');
                 setTimeout(function() {
                      // Do something after 5 seconds
                      location.reload();//reload page
                    }, 4000);
                }
                else
                {
                $('#loginmodal').click();
                }
            }
        });
     return false;
});



$(document).on("keyup",'#booking-price',function(e){
	var price = $('#booking-price').val();
	 $.ajax({
         type:'POST',
         url:"/property/calculate-tax",
         data:{price:price},
         success: function(result)
	        {
	        	$('#booking-tax').val(result.tax);
	        	$('#booking-total').val(result.amount);
	        }
    	});
});

$('#confirm-payment').on('beforeSubmit',function(e){
	e.preventDefault();

	//getting the new value from the textbox
	var amount = $('#booking-total').val();
	var occupancy = $('#booking-occupancy option:selected').val();

	//appending the changed value from text box Into the hash creation
	$('<input />').attr('type', 'hidden')
      .attr('name', "amount")
      .attr('value', amount)
      .appendTo('#confirm-payment');

    // $('<input />').attr('type', 'hidden')
    //   .attr('name', "occupancy")
    //   .attr('value', occupancy)
    //   .appendTo('#confirm-payment');  

	var form  = $('#confirm-payment').serialize();

	$.ajax({
     type:'POST',
     url:"/mub-admin/dashboard/payu-hash",
     data:form,
     async:false,
     success: function(result)
        {
        $('<input />').attr('type', 'hidden')
	      .attr('name', "hash")
	      .attr('value', result.hash)
	      .appendTo('#confirm-payment');	  
	    }
		});
    return true;
});

$("#booking").on('beforeSubmit',function(e){
	e.preventDefault();
	if($('#booking').find('.has-error').length)
	{
		return false;
	}
	else
	{
	$('#confirmPrice').modal('show');
	var form = $('#booking').serialize();
	$.ajax({
     type:'POST',
     url:"/mub-admin/dashboard/save-booking",
     data:form,
     success: function(result)
        {
        	if(result)
        	{
        		 $('#booking').html('<h3 style="text-align:center;">Thank you For Booking.</h3> <br/><p style="text-align:center; padding-bottom: 7.3em; color:#4B829D; font-size:16px;line-height:35px;">We have send you a mail for <br/>confirmation, a copy of which <br/>will be send to the owner,<br/> You can expect a call <br/>from the owner soon.</h4>');
    		        setTimeout(function() {
			      // Do something after 5 seconds
			      location.reload();//reload page
			}, 4000);

        	}
        	else
    		{
        		$('#loginmodal').click();
        	}
        }
   	});
	}
});


$(document).on("click", "#signup-modal", function(event){
     $.ajax({
         type:'GET',
         url:"/site/signup",
         success: function(result)
	        {
	        	$('#dynamic-modal').html(result);
	        }
    	});
});

$(document).on("click", "#forget", function(event){
     $.ajax({
         type:'GET',
         url:"/site/forgetpass",
         success: function(result)
	        {    	
	        	$('#dynamic-modal').html(result);
	        }
    	});
});

$(document).on("submit", "#frontend-forget", function(e){
     e.preventDefault();
     var form = $('#frontend-forget').serialize();
     $.ajax({
         type:'POST',
         url:"/site/forgetpass",
         data:form,
         success: function(result)
	        {
	        	if(result === 'mailsent')
	        	{
		        	$('#frontend-forget').html('<p style="text-align:center; padding-bottom: 7.3em; color:#4B829D; font-size:16px;line-height:35px;">You will Receive the Password for this Account on your Registered Mail ID.</p>');
	        	}
	        	else
	        	{
	        		$('#clientsignup-email').parent().addClass('has-error');
	        		$('.help-block-error').html('<p style="font-size:13px; color:#a94442; margin-top:0px; margin-bottom:-1em;">Please Enter Registered Email</p>');
	        	}
	        }
    	});
     return false;
});

$(document).on("submit", "#frontend-signin", function(e){
     e.preventDefault();
     var form = $('#frontend-signin').serialize();
     $.ajax({
         type:'POST',
         url:"/site/client-login",
         data:form,
         success: function(result)
	        {
	        	$('#dynamic-modal').html(result);
	        }
    	});
     return false;
});

$(document).on("submit", "#frontend-signup", function(e){
     e.preventDefault();
     var form = $('#frontend-signup').serialize();
     $.ajax({
         type:'POST',
         url:"/site/client-register",
         data:form,
         success: function(result)
	        {
	        	$('#dynamic-modal').html(result);
	        }
    	});
     return false;
});

$(document).on("click", "#signin-modal", function(event){
     $.ajax({
         type:'GET',
         url:"/site/client-login",
         success: function(result)
	        {
	        	$('#dynamic-modal').html(result);
	        }
    	});
});

$(document).on('change','#mub-state',function(){
    var selected = $("#mub-state option:selected").val();
    $.ajax({
         type:'POST',
         url:"/mub-admin/dashboard/get-city",
         data:{stateId:selected},
        success: function(result){
            $('#homeslisting-city_name').children("option").remove();
            $('#homeslisting-city_name').prop("disabled", false);
            $('#homeslisting-city_name').append(new Option('Select', ''));
        $.each(result.result, function (i, item) {
            	$('#homeslisting-city_name').append(new Option(item.city_name, item.id));
        	});}
    	}); 
 	});

$(function () {
	var today = new Date();
	var dd = today.getDate();

	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	var today = yyyy+'/'+mm+'/'+dd;
	if($('#datetimepicker1').length !== 0)
	{
		$('#datetimepicker1').datetimepicker({
	      	minDate : {
	        	        Default: today,
	        	    },
	        format:'YYYY-MM-DD HH:mm:ss',
	           });	
	}

      $(window).scroll(function() {
        var x = $(window).scrollTop();
        if (x >= 100) {
            $(".hid_logo").show();
        } else {
            $(".hid_logo").hide(); 
        }
      });
 });
//making sure user selects value from dropdown
$('#search-property').on('click',function(){
	var lat = $('#lat').val();
	
	if(lat === '')
	{
		$('#error-search-field').text('Please select a Location from Drop Down');
		
		return false;
	}
	$('#home-main-search').submit();
});

var openNav = function() {
    document.getElementById("searchTab").style.width = "300px";
}

var closeNav = function() {
    document.getElementById("searchTab").style.width = "0";
}

function initialize() {
	var options = {
	types: ['(regions)'],
	componentRestrictions: {country: "in"}
	};
	var places = document.getElementById("searchPlacesPg");
    var placesnew = document.getElementById("searchPlacesHouse");
	if(places !== undefined){
	   var autocompletePg = new google.maps.places.Autocomplete(places, options);
    }
    if(placesnew !== undefined)
    {
      var autocompleteHouse = new google.maps.places.Autocomplete(placesnew, options);
    }
	var stateName = null;
    var pincode = null;
    var locality = null;
    
    if(places !== null)
    {
        //EventListener for PG Tab
        google.maps.event.addListener(autocompletePg, 'place_changed', function() {
            var place = autocompletePg.getPlace();
            console.log(JSON.stringify(place.address_components));
            for (var i = place.address_components.length - 1; i >= 0; i--) 
            {
                if(place.address_components[i].types[0] == 'administrative_area_level_1')
                {
                    stateName = place.address_components[i].long_name;
                }

                if(place.address_components[i].types[0] == 'postal_code')
                {
                    pincode = place.address_components[i].long_name;
                }

                if(place.address_components[i].types[0] == 'sublocality_level_1')
                {
                    locality = place.address_components[i].long_name;
                }
            }

        var components = this.getPlace().address_components,city='n/a';
            //Getting The City Name
        if(components){
            for(var c=0;c<components.length;++c){
              if(components[c].types.indexOf('locality')>-1
                  &&
                 components[c].types.indexOf('political')>-1
                ){
                city=components[c].long_name;
                break;
              }
            }
          }
        var lat = place.geometry['location'].lat();
        var lng = place.geometry['location'].lng();
        document.getElementById("city_name").value = city;
        document.getElementById("lat").value = lat;
        document.getElementById("long").value = lng;        
        document.getElementById('state_name').value = stateName;
        document.getElementById('pincode').value = pincode;
        document.getElementById('locality').value = locality;
        var page = document.getElementById("page_name").value;
        if(page == 'searchresult')
        {
            places.click();
        }
        });
    }

    if(placesnew != null)
    {    
        //EventListener for House Tab
        google.maps.event.addListener(autocompleteHouse, 'place_changed', function() {
            var place = autocompleteHouse.getPlace();
            console.log(JSON.stringify(place.address_components));
            for (var i = place.address_components.length - 1; i >= 0; i--) 
            {
                if(place.address_components[i].types[0] == 'administrative_area_level_1')
                {
                    stateName = place.address_components[i].long_name;
                }

                if(place.address_components[i].types[0] == 'postal_code')
                {
                    pincode = place.address_components[i].long_name;
                }

                if(place.address_components[i].types[0] == 'sublocality_level_1')
                {
                    locality = place.address_components[i].long_name;
                }
            }

        var components = this.getPlace().address_components,city='n/a';
            //Getting The City Name
        if(components){
            for(var c=0;c<components.length;++c){
              if(components[c].types.indexOf('locality')>-1
                  &&
                 components[c].types.indexOf('political')>-1
                ){
                city=components[c].long_name;
                break;
              }
            }
          }
            var lat = place.geometry['location'].lat();
            var lng = place.geometry['location'].lng();
            document.getElementById("city_name").value = city;
            document.getElementById("lat").value = lat;
            document.getElementById("long").value = lng;        
            document.getElementById('state_name').value = stateName;
            document.getElementById('pincode').value = pincode;
            document.getElementById('locality').value = locality;
            var page = document.getElementById("page_name").value;
            if(page == 'searchresult')
            {
                placesnew.click(); 
            }
            });
    }
    //initializing the FaceBook app
    window.fbAsyncInit = function() {
        FB.init({
          appId      : '1525831840844928',
          cookie     : true,
          xfbml      : true,
          version    : '2.10'
        });
        FB.AppEvents.logPageView();         
    };

	
	//creating map amd adding Marker
	var canvas = document.getElementById('map-single');
	if(canvas !== null)
	{
		function addMarker(location, map) {
		  var marker = new google.maps.Marker({
		    position: location,
		    title: 'Home Center',
            icon: '/images/pin.png',
		    map: map
		  });
		}
		var latitude = parseFloat(document.getElementById('map-lat').value);
		var longitude = parseFloat(document.getElementById('map-lon').value);
		var myLatLng = {
		    lat: latitude,
		    lng: longitude
		}
		var mapOptions = {
		    center: new google.maps.LatLng(latitude, longitude),
		    zoom: 15,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
  		}
		var map = new google.maps.Map(canvas, mapOptions);

		addMarker(myLatLng, map);
	}
}

google.maps.event.addDomListener(window, 'load', initialize);

var placesArray = document.getElementsByClassName("searchPlaces");

Array.prototype.forEach.call(placesArray,function(places){
    if(places !== null)
    {
        google.maps.event.addDomListener(places, 'keydown', function(e) { 
            if (e.keyCode == 13) 
            { 
                e.preventDefault(); 
            }
        });     
    }
});

//Facebook Library Integration
(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }
   (document, 'script', 'facebook-jssdk')
);


jQuery(function($) {
  function fixDiv() {
    var $cache = $('#filterbox');
    var pg = $('#rooms_type').val();
    if ($(window).scrollTop() > 120){
        if(pg=='pg'){
            $('#search_record').addClass('filt_height');
            $('#left_button').removeClass('dspl_show');
        }
        $('#search_record').addClass('search_filters');
       
    }else{
        $('#search_record').removeClass('search_filters');
        $('#search_record').removeClass('filt_height');
        $('#left_button').addClass('dspl_show');
    }
  }
  $(window).scroll(fixDiv);
  fixDiv();
});


$('#newsletter-form').on('submit',function(e){
    e.preventDefault();
    var email = $('#email_id').val();
    $.ajax({
        url:"/site/subscribe",
        data: {'email':email},
        success: function(data)
            {
                if(data==='mailsent')
                {
                   document.getElementById("subscrb").innerHTML = "Thank you";
                }
            }
        });
    return false;
});

$(document).ready(function() {

    $(".side_button").click(function(){       
         jQuery('#search_record').toggleClass('display_filter');
    });
     
});

