ALTER TABLE `post` CHANGE `status` `status` ENUM('active','inactive') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive';
ALTER TABLE `post_detail` CHANGE `read_count` `read_count` INT(11) NOT NULL DEFAULT '0';
UPDATE `post_category` SET `category_id`='5' WHERE 1;