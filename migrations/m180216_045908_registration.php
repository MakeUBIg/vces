<?php

namespace app\migrations;
use app\commands\Migration;
/**
 * Class m180216_045908_registration
 */
class m180216_045908_registration extends Migration
{
    public function getTableName()
    {
        return 'registration';
    }

    public function getKeyFields()
    {
        return [
                'first_name' => 'first_name',
                'email' => 'email',
                'address_line1' => 'address_line1',
                'address_line2' => 'address_line2',
                'town' => 'town',
                'home_phone' => 'home_phone',
                'gender' => 'gender',
                'country' => 'country',
                'year_of_graduation' => 'year_of_graduation',
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(50)->notNull(),
            'middle_name' => $this->string(50),
            'last_name' => $this->string(50),
            'birth_name' => $this->string(50)->notNull(),
            'prefered_name' => $this->string(50),
            'email' => $this->string(50)->notNull(),
            'street_no' => $this->string(50)->notNull(),
            'address_line1' => $this->string(50)->notNull(),
            'address_line2' => $this->string(50)->notNull(),
            'town' => $this->string(50)->notNull(),
            'province' => $this->string(50)->notNull(),
            'gender' => "enum('male','femail') NOT NULL DEFAULT 'male'",
            'postal_zip' => $this->string(50)->notNull(),
            'home_phone' => $this->bigInteger(12)->notNull(),
            'work_telephone' => $this->bigInteger(12),
            'date_of_birth' => $this->string(50)->notNull(),
            'country' => $this->string()->notNull(),
            'first_choice' => $this->string(50)->notNull(),
            'second_choice' => $this->string(50)->notNull(),
            'undecide' => $this->string(50)->notNull(),
            'registring_term' => $this->string(50)->notNull(),
            'immediate_education' => $this->string(50)->notNull(),
            'applying' => $this->string(50)->notNull(),
            'first_launguage' => $this->string(50)->notNull(),
            'launguage_thoughout' => $this->string(50)->notNull(),
            'iltes_score' => $this->string(50)->notNull(),
            'tofel_score' => $this->string(50)->notNull(),
            'gmat_score' => $this->string(50)->notNull(),
            'official_school_name' => $this->string(50)->notNull(),
            'town_city' => $this->string(50)->notNull(),
            'province_state' => $this->string(50)->notNull(),
            'from' => $this->string()->notNull(),
            'to' => $this->string()->notNull(),
            'year_of_graduation' => $this->integer()->notNull(),
            'i_agree' => $this->string()->notNull(),
        ];
    }
}