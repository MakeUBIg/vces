<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        '/css/font-awesome.css',
        '/fi/flaticon.css',
        '/css/main.css',
        '/css/jquery.fancybox.css',
        '/css/owl.carousel.css',
        '/rs-plugin/css/settings.css',
        '/css/animate.css',
            ];

    public $js = [
        '/js/custom_mub_frontend.js',
        '/js/jquery.validate.min.js',
        '/js/jquery.form.min.js',
        '/js/TweenMax.min.js',
        '/js/main.js',
        '/rs-plugin/js/jquery.themepunch.tools.min.js',
        '/rs-plugin/js/jquery.themepunch.revolution.min.js',
        '/rs-plugin/js/extensions/revolution.extension.video.min.js',
        '/rs-plugin/js/extensions/revolution.extension.slideanims.min.js',
        '/rs-plugin/js/extensions/revolution.extension.actions.min.js',
        '/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js',
        '/rs-plugin/js/extensions/revolution.extension.kenburn.min.js',
        '/rs-plugin/js/extensions/revolution.extension.navigation.min.js',
        '/rs-plugin/js/extensions/revolution.extension.migration.min.js',
        '/rs-plugin/js/extensions/revolution.extension.parallax.min.js',
        '/js/jquery.isotope.min.js',
        '/js/owl.carousel.min.js',
        // '/js/jquery-ui.min.js',
        '/js/jflickrfeed.min.js',
        '/js/jquery.fancybox.pack.js',
        '/js/jquery.fancybox-media.js',
        '/js/retina.min.js',
        '/js/jquery.tweet.js',
        
    ];
     
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
