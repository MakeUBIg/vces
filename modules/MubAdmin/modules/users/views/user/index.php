<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MubUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add New User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'first_name',
            'last_name',
            [
            'label' => 'Mobile',
            'attribute'=>'mobile',
            'value' => 'mubUserContacts.mobile',
            ],
            [
            'label' => 'Address', 
            'attribute'=>'address',
            'value' => 'mubUserContacts.address',
            ],
            'username',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $userRole = \Yii::$app->controller->getUserRole();
                    if($userRole == 'admin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'Active';
                    $id = $model->id;
                    $valInactive = 'Inactive';
                    return ($model->status == 'Inactive') ? '<button onClick="changeUserStatus('.'\''.$attrib.'\''.','.'\''.$valActive.'\''.','.'\''.$id.'\''.')">Activate</button>' : '<button onClick="changeUserStatus('.'\''.$attrib.'\''.','.'\''.$valInactive.'\''.','.'\''.$id.'\''.')">Deactivate</button>';    
                    }
                    else
                    {
                        return $model->status;
                    }
                    
                },
            ],
            // 'del_status',

            ['header' => 'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>