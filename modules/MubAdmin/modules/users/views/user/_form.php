<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\City;
if(Yii::$app->controller->action->id == 'update')
{
    $city = $mubUserContacts->city;
    $cityModel = new City();
    $state = $cityModel::findOne($city)->state_id;
    $allCities = $cityModel->getManyById($state,'state_id','city_name');
    $status = $mubUser->status;
}
?>


<div class="mub-user-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($mubUser, 'first_name')->textInput(['maxlength' => true]) ?>
        
    </div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($mubUser, 'last_name')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($mubUserContacts, 'mobile')->textInput(['maxlength' => '10']) ?>
    </div>
<div class="col-md-5 col-sm-12 col-xs-12">

    <?= $form->field($mubUserContacts, 'address')->textInput(['maxlength' => true])?>

</div>
</div>
<div class="row">
<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($mubUserContacts, 'address')->textInput();?>

</div>
</div>
<div class="row">
    <?php 
    if(Yii::$app->controller->action->id == 'update')
{?>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($mubUserContacts, 'state')->dropDownList($allStates,['options' =>
                    [                        
                      $state => ['selected' => true]
                    ]
          ],['id' => 'mub-state','prompt' => 'Select A state']);?>
   </div>
   <div class="col-md-5 col-sm-12 col-xs-12">

    <?= $form->field($mubUserContacts, 'city')->dropDownList($allCities, ['prompt' => 'Select A City']);?>
    </div>
<?php }
else
{ ?>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
   <?= $form->field($mubUserContacts, 'state')->dropDownList($allStates, ['id' => 'mub-state','prompt' => 'Select A state']);?>
</div>

<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($mubUserContacts, 'city')->dropDownList([], ['prompt' => 'Select A City']);?>
    </div>
 <?php }
    ?>
</div>
<div class="row">
   <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($mubUser, 'organization')->textInput()->label('Company Name');?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($mubUser, 'domain')->textInput();?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
      <?= $form->field($mubUser, 'username')->textInput(['maxlength' => true, 'readonly' => (Yii::$app->controller->action->id == 'update') ? true : false]);?>
      </div>
      <?php if(Yii::$app->controller->action->id == 'update'){
    ?>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($mubUser, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'InActive'], ['prompt' => 'Select A Status']);?>
    </div>
    <?php }else{?>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($mubUser, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'InActive'],['prompt' => 'Select User Status']);?>
    </div>
    <?php }?>
    </div>
    <div class="row">
     <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
     <?= $form->field($mubUser, 'password')->passwordInput(['maxlength' => true]);?>
    <?php if(Yii::$app->controller->action->id == 'create'){?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($mubUser, 'password_confirm')->passwordInput(['maxlength' => true]);?>
    </div>
    <?php }?>
    </div>
    <div class="col-md-5 col-md-offset-1">
    <?= $form->field($mubUserStates, 'state_id')->checkboxList($activeStates)->label("State of Operations (You can select Multiple)");?>
    </div>

    <div class="form-group">
<div class="col-md-12" style="text-align: center;" >
        <?= Html::submitButton($mubUser->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $mubUser->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
