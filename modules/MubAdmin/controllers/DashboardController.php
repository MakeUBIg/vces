<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\City;
use app\models\MubUser;
use app\models\HomesListing;
use app\components\Model;

class DashboardController extends MubController
{
	public function getProcessModel()
	{

	}

	public function getPrimaryModel()
	{

	}

	public function getDataProviders()
	{
		return [];
	}
    public function actionSaveEnquiry()
    {
        if(Yii::$app->request->isAjax) 
        {
            $mubUserId = \app\models\User::getMubUserId();
            if($mubUserId)
            {
                $formData = \Yii::$app->request->getBodyParams();
                if(isset($formData['ScheduledVisits']))
                {
                    $scheduledVisits = new \app\models\ScheduledVisits();
                    $allreadyExists = $scheduledVisits::find()->where(['property_id' => $formData['ScheduledVisits']['property_id'],'mub_user_id' => $mubUserId,'status' => 'scheduled','del_status' => 0,'occupancy' => $formData['ScheduledVisits']['occupancy']])->all();
                    if(empty($allreadyExists))
                    {
                        if($scheduledVisits->load($formData))
                        {
                            $scheduledVisits->mub_user_id = $mubUserId;
                            if(!$scheduledVisits->save())
                            {
                                p($scheduledVisits->getErrors());
                            }
                            $propertyModel = new \app\modules\MubAdmin\modules\RealEstate\models\Property();
                            $property = $propertyModel::findOne($scheduledVisits['property_id']);
                            $ownerContact = MubUser::findOne($property->mub_user_id)->mubUserContacts;
                            \Yii::$app->mailer->compose('schedulevisit',['property' => $property,'schedule' => $scheduledVisits])->setFrom('info@osmstays.com')->setCc('info@osmstays.com')->setBcc('admin@makeubig.com')->setTo([$formData['ScheduledVisits']['email']])->setSubject('Your visit is scheduled for '. $property->property_name . ' at '. $formData['ScheduledVisits']['scheduled_time'])->send();
                            return $scheduledVisits->id;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }
    public function saveBookingDetails($postData)
    {

        if(!empty($postData))
        {
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking::deleteAll('booking_id = '."'".$postData['txnid']."'");
            $booking->name = $postData['firstname'];
            $booking->mub_user_id = $mubUserId;
            $booking->property_id = $postData['propertyId'];
            $booking->email = $postData['email'];
            $booking->mobile = $postData['phone'];
            $booking->occupancy = $postData['occupancy'];
            $booking->status = 'booking';
            $booking->booking_id = $postData['txnid'];
            $booking->amount = $postData['amount'];
            $booking->actual_price = $postData['actual_price'];
            $booking->tax_amount = intval($postData['amount'] - $postData['actual_price']);
            if(!$booking->save())
            {
               p($booking->getErrors());
            }
        }
        return true;
    }
	public function actionGetCity() {
        if (Yii::$app->request->isAjax) {
            $stateId = Yii::$app->request->post('stateId');
            // $data = [];
            $result = City::find()->where(['state_id' => $stateId])->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => $result
            ];
        } else {
            return false;
        }
    }

    public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }

    public function actionListHome()
    {
        if (Yii::$app->request->isAjax)
        {
        $formData = \Yii::$app->request->getBodyParams();
        if(isset($formData['HomesListing']))
        {
            $listHome = new \app\models\HomesListing();
            if($listHome->load($formData))
            {
                if(!$listHome->save())
                {
                    p($listHome->getErrors());
                }
                // $homelistModel = new \app\modules\MubAdmin\modules\RealEstate\models\Property();
                // $listhome = $homelistModel::findOne($listHome['mub_user_id']);
               
                \Yii::$app->mailer->compose('Listhomemail',['listHome' => $listHome])
                    ->setFrom('info@osmstays.com')
                    ->setCc('info@osmstays.com')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->setTo([$formData['HomesListing']['email'],$listHome->email])->setSubject('Your Listed Home')
                    ->send();       
                return $listHome->id;
            }
        }
        }
        return false;
    }

    public function actionPayuHash()
    {
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            $postData = \Yii::$app->request->getBodyParams();
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';  
            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($postData[$hash_var]) ? $postData[$hash_var] : '';
                $hash_string .= '|';
            }
          $hash_string .= 'RK4rlkOAr8';
          $result['hash'] = strtolower(hash('sha512', $hash_string));

          $this->saveBookingDetails($postData);
          
          return $result;
        }
    }


}