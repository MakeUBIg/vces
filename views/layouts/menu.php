<?php 

$menu = [];
if(\Yii::$app->user->can('dashboard/index'))
{
    $menu[] = ["label" => "Dashboard", "url" => "/mub-admin", "icon" => "home"];        
}

if(\Yii::$app->user->can('brokeritem/index'))
{
    $menu[] = ["label" => "Brokers", "url" => "/mub-admin/broker/brokeritem/", "icon" => "cog"];        
}


if(\Yii::$app->user->can('album/index'))
{
$menu[] = [
            "label" => "Gallery", 
            "url" => ["/mub-admin/gallery"], 
            "icon" => "picture-o"
        ];
}

if(\Yii::$app->user->can('user/index'))
{
     $userss = new  app\models\MubUser();

    $newCount = $userss::find()->where(['del_status' => '0','status' => 'inactive'])->count();
    $userssCount = $userss::find()->where(['del_status' => '0','status' => 'active'])->count();
    $menu[] = ["label" => "Users", "url" => ["/mub-admin/users"], "icon" => "user",
                "badge" => ($newCount > 0) ? $newCount : $userssCount,
                "badgeOptions" => ["class" => ($newCount > 0) ?"label-success" : ''],
                "icon" => "cog"
            ];
}

?>
