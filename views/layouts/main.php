<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
$action  = \Yii::$app->controller->action->id;
// p($action);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>

<body>
    <!-- page header -->
    <header class="only-color">
        <!-- header top panel -->
        <div class="page-header-top">
            <div class="grid-row clear-fix">
                <address>
                    <a href="#" class="phone-number"><i class="fa fa-phone"></i>+91 9810293046  <i class="fa fa-phone"></i> 009028097789</a>
                    <a href="mailto:info@rsiapl.in" class="email"><i class="fa fa-envelope-o"></i>info@vces.ca</a>
                </address>
                <div class="header-top-panel">
                   
                </div>
            </div>
        </div>
        <!-- / header top panel -->
        <!-- sticky menu -->
        <div class="sticky-wrapper">
            <div class="sticky-menu">
                <div class="grid-row clear-fix">
                    <!-- logo -->
                    <a href="/" class="logo">
                        <img src="/img/logo2.png"  data-at2x="img/logo@2x.png" alt>
                        <h1></h1>
                    </a>
                    <!-- / logo -->
                    <nav class="main-nav">
                        <ul class="clear-fix">
                            <li>
                                <a href="/" class="<?=($action == 'index') ? 'active' : '';?>">Home</a>
                            </li>
                            <li>
                                <a href="" class="<?=(in_array($action,['about','our-team'])) ? 'active' : '';?>" >About Us</a>
                                <!-- sub menu -->
                                <ul>
                                    <li><a href="/site/about">Our Values</a></li>
                                    <li><a href="/site/our-team">Our Team</a></li>
                                </ul>
                                <!-- / sub menu -->
                            </li>
                            <li>
                                <a href="" class="<?=(in_array($action,['undergraducate','graducate'])) ? 'active' : '';?>">Courses</a>
                                <ul>
                                    <li><a href="/site/undergraducate">Undergraduate Programmes</a></li>
                                    <li><a href="/site/graducate">Graduate Programmes</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/site/partnerunivr" class="<?=($action == 'partnerunivr') ? 'active' : '';?>" >University/College</a>
                                <ul>
                                    <li><a href="/site/partnerunivr">University/College</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="" class="<?=(in_array($action,['addmissionprocess','procedure','fee'])) ? 'active' : '';?>">Admission</a>
                                <!-- sub menu -->
                                <ul>
                                    <li><a href="/site/addmissionprocess">Admission Requirements</a></li>
                                    <li><a href="/site/procedure">Admission procedure</a></li>
                                    <li><a href="/site/fee">Fees</a></li>
                                    <li><a href="/site/registration">Registration</a></li>
                                </ul>
                                <!-- / sub menu -->
                            </li>
                            <li>
                                <a href="" class="<?=(in_array($action,['immegration','servicsupport'])) ? 'active' : '';?>">Services</a>
                                <!-- sub menu -->
                                <ul>
                                    <li><a href="/site/immegration">Immigration Services</a></li>
                                    <li><a href="/site/servicsupport">We Support You In</a></li>
                                </ul>
                                <!-- / sub menu -->
                            </li>
                            <li>
                                <a href="/site/contact">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
       </header>
<?= $content ?>

    <!-- footer -->
    <footer>
        <div class="grid-row">
            <div class="grid-col-row clear-fix">
                <section class="grid-col grid-col-4 footer-about">
                    <h2 class="corner-radius">About Us</h2>
                    <div>
                        <h3>VCES is set up to offer reliable and honest services</h3>
                        <p> supporting the prospective applicants who wish to pursue higher education or wish to immigrate to Canada. VECS is formed by the Ex Georgians to support fellow Georgians / Wards who wish to apply in Canadian Universities & Colleges.</p>
                    </div>
                    <address>
                        <p></p>
                        <a href="tel:123-123456789" class="phone-number">91 9810293046</a>
                        <br />
                        <a href="mailto:uni@domain.com" class="email">vcescanada@gmail.com</a>
                        <br />
                        <a href="www.sample.com" class="address">Canada - 153 Pebblecreek Crescent, Dartmouth, Nova Scotia, Canada</a><br>
                        <a href="www.sample.com" class="address">India - H – 23/13, DLF – I, Gurgaon</a>
                    </address>
                    <div class="footer-social">
                        <a href="" class="fa fa-twitter"></a>
                        <a href="" class="fa fa-skype"></a>
                        <a href="" class="fa fa-google-plus"></a>
                        <a href="" class="fa fa-rss"></a>
                        <a href="" class="fa fa-youtube"></a>
                    </div>
                </section>
                <section class="grid-col grid-col-4 footer-latest">
                    <h2 class="corner-radius">Latest courses</h2>
                    <article>
                        <img src="/img/book12.jpg" data-at2x="http://placehold.it/83x83" alt>
                        <h3>Undergraduate (Bachelor) Programs</h3>
                        <!-- <div class="course-date">
                            <div>10<sup>00</sup></div>
                            <div>23.02.15</div>
                        </div> -->
                        <p>We provide undergraduate program through all over the world from an approved university.</p>
                    </article>
                    <article>
                        <img src="/img/cap12.jpg" data-at2x="http://placehold.it/83x83" alt>
                        <h3>Graduate (Master) Programs</h3>
                        <!-- <div class="course-date">
                            <div>10<sup>00</sup></div>
                            <div>23.02.15</div>
                        </div> -->
                        <p>Candidates for admission to the graduate programs of most Universities must have an honor’s degree from an approved university.</p>
                    </article>
                </section>
                <section class="grid-col grid-col-4 footer-contact-form">
                    <h2 class="corner-radius">apply for instructor</h2>
                    <div class="email_server_responce"></div>
                    <form action="php/contacts-process.php" class="contact-form" method="post" novalidate="novalidate">
                        <p><span class="your-name"><input type="text" name="name" value="" size="40" placeholder="Name" aria-invalid="false" required></span>
                        </p>
                        <p><span class="your-email"><input type="text" name="phone" value="" size="40" placeholder="Phone" aria-invalid="false" required></span> </p>
                        <p><span class="your-message"><textarea name="message" placeholder="Comments" cols="40" rows="5" aria-invalid="false" required></textarea></span> </p>
                        <button type="submit" class="cws-button bt-color-3 border-radius alt icon-right">Submit <i class="fa fa-angle-right"></i></button>
                    </form>
                </section>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="grid-row clear-fix">
                <div class="copyright">vces<span></span> 2018 .Made By MakeUBig</div>
                <!-- footer nav -->
                <nav class="footer-nav">
                    <ul class="clear-fix">
                        <li>
                            <a href="/">Home</a>
                        </li>
<!--                         <li>
                            <a href="/site/">Courses</a>
                        </li> -->
                        <li>
                            <a href="/site/undergraducate">Undergraduate Programs</a>
                        </li>
                        <li>
                            <a href="/site/graducate">Graduate Programs</a>
                        </li>
                        <li>
                            <a href="/site/immegration">Immigration Services</a>
                        </li>
                        <li>
                            <a href="/site/servicsupport">We Support In</a>
                        </li>
                        <!--  -->
                        <li>
                            <a href="/site/contact">Contact</a>
                        </li>
                    </ul>
                </nav>
                <!-- footer nav -->
            </div>
        </div>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
