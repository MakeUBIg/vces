	
        <div class="page-title">
            <div class="grid-row">
                <h1>Our Team</h1>
                <nav class="bread-crumb">
                    <a href="index.html">Home</a>
                    <i class="fa fa-long-arrow-right"></i>
                    <a href="">Our Team</a>
                </nav>
            </div>
        </div>

	<div class="page-content">
		<main>
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-8">
							<h2>Gurvinder Mann</h2>, <b>Certified Internal Auditor, Director- Canada Operation, Halifax Office</b><br>
							<p>A proud alumnus of Dholpur Military School, Dholpur (1986 – 1993). He is a Certified Internal Auditor (CIA) and is enjoying a successful career with the Nova Scotia Government (11 years).</p>
							<p>During this time, he has gained an in-depth understanding of Canadian policy and processes through his work in immigration dept., economic development, business planning, and public administration.</p>
						</div>
						<div class="grid-col grid-col-4">
							<div class="owl-carousel full-width-slider">
								<div class="gallery-item picture">
									<img src="/img/GurvinderMan.jpg" data-at2x="http://placehold.it/570x380" alt>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<hr class="divider-color" />
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-4">
							<div class="gallery-item picture">
								<img src="/img/JagpalMann.jpg" data-at2x="http://placehold.it/570x380" alt>
							</div>
						</div>
						<div class="grid-col grid-col-8">
							<h2>Jagpal Mann LLB</h2>
							<div class="grid-col grid-col-2"><img src="/img/1.png" style="height: 140px;" alt></div><div class="grid-col grid-col-2"><img src="/img/2.jpg" style="height: 140px;" alt></div><div class="grid-col grid-col-2"><img src="/img/3.gif" style="height: 140px;" alt></div><br><br><br><br><br><br><br> <b>Barrister & Solicitor, Director – Canada Operations, Toronto Office</b><br>
							<p>Jagpal Mann is a lawyer in the Province of Ontario, Canada and is a member of the Law Society of Upper Canada and Canadian Bar Association.</p>
							<p>He advises on a wide variety of matters related to applications for Canadian immigration. Mr. Mann’s primary role as lawyer within the firm is to determine your eligibility for immigration to Canada.</p>
						</div>
						<div class="grid-col grid-col-8">
						<h5></h5>
							<p>He also helps people to choose the category of Canada immigration which best suits their qualifications and aspirations. Mr. Mann would be supervising each candidate’s application process from start to finish and would also communicate with government regulatory bodies during the Application for Permanent Canadian Residence process. </p>
							<p>His practice philosophy is to offer highly personalized immigration service in an efficient and cost effective manner. Full legal representation is extended from the initial stages of process till your arrival in Canada.</p>
						</div>
					</div>
				</div>
			</section>
			<hr class="divider-color" />
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-8">
							<h2>Siddharth Tomar</h2>,<b>Director- India Operations (New Delhi)</b><br>
							<p>Studied at Dholpur Military School, Dholpur (1986 – 1993), completed his B.Com (Agra University) in 1996 & Post Graduate in Commerce from St. Johns’ College, Agra in 1998.</p>
							<p>Part of a leading HR resourcing organization since November 1999 onwards, has worked with clients include both Indian & Multinational companies.</p>
							<p>Understands the hiring practices in India and can provide expert guidance in choosing a career for future.</p>
						</div>
						<div class="grid-col grid-col-4">
							<div class="owl-carousel full-width-slider">
								<div class="gallery-item picture">
									<img src="/img/siddarthtomar.jpg" data-at2x="http://placehold.it/570x380" alt>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<hr class="divider-color" />
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-4">
							<div class="owl-carousel full-width-slider">
								<div class="gallery-item picture">
									<img src="/img/sanjay.jpg" data-at2x="http://placehold.it/570x380" alt>
								</div>
							</div>
						</div>
						<div class="grid-col grid-col-8">
							<h2>Capt. Sanjay Singh (Retd.) (Georgian)</h2>,<b> Director- India Operations (Bengaluru)</b>
							<p>Sanjay an alumni of Military School Bangalore (1987 – 1989) and Military School Dholpur (1989 – 1994) has 20+ years of progressive career under his belt in various fields including 6 yrs in Indian Army as Captain and 7 Yrs of International Exposure in Banking and Retail space.</p>
							<p>He is also a double MBA from IIM, Indore (Ops Management) and IIMM, Pune (HR & PM). Currently he is settled in Bangalore as an Infrastructure Developer and is into developing affordable housing space.</p>
						</div>
					</div>
				</div>
			</section>

		</main>
	</div>