
<div class="page-title">
	<div class="grid-row">
		<h1>Tuition & Fees</h1>
		<nav class="bread-crumb">
			<a href="index.html">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="">Tuition & Fees</a>
		</nav>
	</div>
</div>

<div class="page-content">
	<div class="container">
		<main>
			<h3>Tuition and fees for most full-time undergraduate students are as follows:</h3>
			<p>$19,000 (approx.) per year and includes the following items: Tuition, Information & Technology fee, and Health insurance fee. Does not include living expenses.</p>
			<h5>Tuition and fees for most full-time graduate students are as follows:</h5>
			<p>$25,000 (approx.) per year and includes the following items: Tuition, Information & Technology fee, and Health insurance fee. Does not include living expenses.</p>
		</main>
	</div>
</div>