
<div class="page-title">
	<div class="grid-row">
		<h1>Support</h1>
		<!-- bread crumb -->
		<nav class="bread-crumb">
			<a href="index.html">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="">Support</a>
		</nav>
		<!-- / bread crumb -->
	</div>
</div>

<div class="page-content">
		<div class="container clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6">
					<!-- main content -->
					<main>
						<!-- item -->
						<div class="category-item list clear-fix">
							<h3>Support:</h3>
							<ul>
								<li>Choosing the university or college</li>
								<li>Completing the application</li>
								<li>Visa support</li>
								<li>Travel arrangements</li>
								<li>Accommodation arrangements</li>
								<li>Career Counselling (aid in finding the part time jobs during studying)</li>
								<li>Permanent immigration support *</li>
								<li>Legal aid (if needed)</li>
							</ul>
							<p>* VCES is different from the other business who are operating in this field. Most of the businesses don’t’ have their own in-house immigration lawyer to provide study visa or permanent immigration support and the Director who has been working for the Government in Canada for more than a decade. 
							</p>
						</div>
					</main>
				</div>
				<div class="grid-col grid-col-6">
					<div class="gallery-item picture">
						<img src="/img/driveway.jpg" data-at2x="/img/driveway.jpg" alt>
					</div>
				</div>
				<!-- / side bar -->
			</div>
		</div>
	</div>