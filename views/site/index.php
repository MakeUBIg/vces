<div class="tp-banner-container">
        <div class="tp-banner-slider">
            <ul>
                <li data-masterspeed="700">
                    <img src="rs-plugin/assets/loader.gif" data-lazyload="/img/Untitled.jpg" alt>
                    <div class="tp-caption sl-content align-left" data-x="['left','center','center','center']" data-hoffset="20" data-y="center" data-voffset="0"  data-width="['720px','600px','500px','300px']" data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" 
     data-transform_out="opacity:0;s:300;s:1000;" data-start="400">
                        <div class="sl-title">Get Ahead</div>
                        <p>With the best qualification<br/>Beat the competition and get ahead of the masses.</p>
                        <a href="" class="cws-button border-radius">Join us <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </li>
                <li data-masterspeed="700">
                    <img src="rs-plugin/assets/loader.gif" data-lazyload="/img/university.jpg" alt>
                    <div class="tp-caption sl-content align-right" data-x="['right','center','center','center']" data-hoffset="20" data-y="center" data-voffset="0"  data-width="['720px','600px','500px','300px']" data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" 
     data-transform_out="opacity:0;s:300;s:1000;" data-start="400">
                        <div class="sl-title">Vast Range </div>
                        <p>More than 50 courses and 11 <br/>Universities to chose from for your career enhancement.</p>
                        <a href="" class="cws-button border-radius">Join us <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- / revolution slider -->
    <hr class="divider-color">
    <div class="page-content">
        <div class="container clear-fix">
            <div class="grid-col-row">
                <div class="grid-col grid-col-6">
                    <!-- main content -->
                    <main>
                        <!-- item -->
                        <div class="category-item list clear-fix">
                            <h3>Education: </h3><br>
                            <p>Want to pursue education in Canada. Look no further, Valiant Canada Education Services is a consultancy service based in Halifax and Toronto, Canada with offices in India (Delhi and Bengaluru).
                            </p>
                            <p>We help students like you to choose a program and educational institution that not only fits your interests, but will also help you to develop a secure career plan.Our counsellors will support you from beginning to end, using an integrated approach that draws on their deep understanding of the process. They’ll also tap into their personal experience of moving from India to study and work in Canada and permanent settling here. 
                    </p>
                        </div>
                    </main>
                </div>
                <div class="grid-col grid-col-6">
                    <div class="gallery-item picture">
                        <img src="/img/People.jpg" data-at2x="/img/People.jpg" alt>
                    </div>
                </div>
                <!-- / side bar -->
            </div>
        </div>
    </div>
    <hr class="divider-color">
    <!-- content -->
    <div id="home" class="page-content padding-none">
        <!-- section -->
        <section class="padding-section mart">
            <div class="grid-row clear-fix">
                <h2 class="center-text">University and College</h2>
                <div class="grid-col-row">
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/DalhousieUniversity.jpg" data-at2x="/img/DalhousieUniversity.jpg" alt="">
                                <div class="hover-bg bg-color-1"></div>
                                <a href="/site/products/#product1" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Dalhousie University</a></h3>
                                </div>
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/AcadiaUniversity1.jpg" data-at2x="/img/AcadiaUniversity1.jpg" alt="">
                                <div class="hover-bg bg-color-2"></div>
                                <a href="/site/products/#product2" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Acadia University</a></h3>
                                </div>
                           
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/MountSentVinceUniversity.jpg" data-at2x="/img/MountSentVinceUniversity.jpg" alt="">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/products/#product3" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">Mount Saint Vincent University</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/Nova-Scotia-Community-College.jpg" data-at2x="/img/Nova-Scotia-Community-College.jpg" alt="">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/products/#product3" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">
                             St Frances Xavier University</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>

                </div>
            </div>
             <section class="padding-section mart">
            <div class="grid-row clear-fix">
                <a href="/site/partnerunivr"><h2 class="center-text"><button  class="cws-button bt-color-3 border-radius alt icon-right" class="butt">View all University and College <i class="fa fa-angle-right" ></i></button></h2></a>
                </div></section>
        </section>
    </div>

    <hr class="divider-color">
    <!-- content -->
    <div id="home" class="page-content padding-none">
        <!-- section -->
        <section class="padding-section mart">
            <div class="grid-row clear-fix">
                <h2 class="center-text">Programmes</h2>
                <div class="grid-col-row">
                    <h4>Undergraduate Programmes</h4>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/books.jpg" data-at2x="/img/books.jpg" alt="">
                                <div class="hover-bg bg-color-1"></div>
                                <a href="/site/undergraducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Applied Science (BASc, CAS)</a></h3>
                                </div>
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/books.jpg" data-at2x="/img/books.jpg" alt="">
                                <div class="hover-bg bg-color-2"></div>
                                <a href="/site/undergraducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Economics (BA)</a></h3>
                                </div>
                           
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/books.jpg" data-at2x="/img/books.jpg" alt="">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/undergraducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">Computer Science (BCS, BSc)</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/books.jpg" data-at2x="/img/books.jpg" alt="">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/undergraducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">Business Administration (BBA)</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>

                </div>
            </div>
             <section class="padding-section mart">
            <div class="grid-row clear-fix">
               <a href="/site/undergraducate"> <h2 class="center-text"><button  class="cws-button bt-color-3 border-radius alt icon-right" class="butt">View all Undergraduate Programmes <i class="fa fa-angle-right" ></i></button></h2></a>
                </div></section>
        </section>
    </div>
    <hr class="divider-color">
    <!-- content -->
    <div id="home" class="page-content padding-none">
        <!-- section -->
        <section class="padding-section mart">
            <div class="grid-row clear-fix">
                <div class="grid-col-row">
                <h4>Graduate Programmes</h4>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/cap.jpg" data-at2x="/img/cap.jpg" alt="">
                                <div class="hover-bg bg-color-1"></div>
                                <a href="/site/graducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Computer Science</a></h3>
                                </div>
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/cap.jpg" data-at2x="/img/cap.jpg" alt="">
                                <div class="hover-bg bg-color-2"></div>
                                <a href="/site/graducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Architecture</a></h3>
                                </div>
                           
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/cap.jpg" data-at2x="/img/cap.jpg" alt="">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/graducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">Pharmacy</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>
                    <div class="grid-col grid-col-3">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/cap.jpg" data-at2x="/img/cap.jpg" alt="">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/graducate" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">Law</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>

                </div>
            </div>
             <section class="padding-section mart">
            <div class="grid-row clear-fix">
               <a href="/site/graducate"> <h2 class="center-text"><button  class="cws-button bt-color-3 border-radius alt icon-right" class="butt">View all Graduate Programmes <i class="fa fa-angle-right" ></i></button></h2></a>
                </div></section>
        </section>
    </div>
    <section class="fullwidth-background padding-section">
        <div class="grid-row clear-fix">
            <div class="grid-col-row">
                <div class="grid-col grid-col-8 clear-fix">
                    <h1>Admission Requirements</h1>
                    <p class="just">All applications must be supported by official copies of all high school transcripts and other records of academic work.</br>
                    Transcripts should be certified by the school and prepared in a sealed envelope by the school. They can either be sent to office directly from the school or given to the student to send. Certified transcripts acceptable will be used as a basis of admission.
                    </p>
                </div>
                <div class="grid-col grid-col-4">
                   <img src="/img/Nova Scotia Community College.jpg">
                </div>
            </div>
        </div>
    </section>
    <hr class="divider-color" />
    <!-- section -->
    <!-- <section class="fullwidth-background testimonial padding-section">
        <div class="grid-row">
            <h2 class="center-text">Testimonials</h2>
            <div class="owl-carousel testimonials-carousel">
                <div class="gallery-item">
                    <div class="quote-avatar-author clear-fix"><img src="http://placehold.it/94x94" data-at2x="http://placehold.it/94x94" alt=""><div class="author-info">Karl Doe<br><span>Writer</span></div></div>
                    <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                </div>
                <div class="gallery-item">
                    <div class="quote-avatar-author clear-fix"><img src="http://placehold.it/94x94" data-at2x="http://placehold.it/94x94" alt=""><div class="author-info">Karl Doe<br><span>Writer</span></div></div>
                    <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                </div>
                <div class="gallery-item">
                    <div class="quote-avatar-author clear-fix"><img src="http://placehold.it/94x94" data-at2x="http://placehold.it/94x94" alt=""><div class="author-info">Karl Doe<br><span>Writer</span></div></div>
                    <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                </div>
            </div>
        </div>
    </section> -->
        <!-- google map -->
        <!-- <div class="wow fadeInUp">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3510.4453835237564!2d76.92458531446006!3d28.375612852414502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d3dd0935f9f8b%3A0x2f34e3d68af3a324!2sR+S+Industries+(Auto+)+Pvt.+Ltd!5e0!3m2!1sen!2sin!4v1514443762907" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div> -->
        <!-- / google map -->
</div>