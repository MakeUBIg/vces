<div class="page-title">
			<div class="grid-row">
				<h1>Thank You !</h1>
				<nav class="bread-crumb">
					<a href="index.html">Home</a>
					<i class="fa fa-long-arrow-right"></i>
					<a href="content-elements.html">Registration Successfully !</a>
					
				</nav>
			</div>
		</div>
	</header>

	<div class="page-content grid-row">
			<main>
				<hr class="divider-color" />
				<section>
					<div class="grid-row">
						<div class="grid-col grid-col-4" style="">
						<b></b><br/>
						
						
					    </div>
					    <div class="grid-col grid-col-4">
						<q><b>Thank you for Provide Your Data</b><br/>
						We will get in touch with you soon.
						</q>
					    </div>
					    
					</div>

				</section>
				
				<section>
					<div class="carousel-container">
						<div class="title-carousel clear-fix">
							<h2>Browse Our Courses</h2>
							<div class="carousel-nav">
								
							</div>
						</div>
						<div class="grid-col-row">
							<div class="owl-carousel owl-two-item">
								<div class="gallery-item">
									<a href="/site/partnerunivr"><img src="/img/MountSentVinceUniversity.jpg"></a><br/>
									<a href="/site/partnerunivr"><h2 class="center-text"><button  class="cws-button bt-color-3 border-radius alt icon-right" class="butt">View all University and College <i class="fa fa-angle-right" ></i></button></h2></a>
								</div>
								<div class="gallery-item">
									<a href="/site/undergraducate"><img src="/img/DalhousieUniversity.jpg"></a><br/>
									<a href="/site/undergraducate"> <h2 class="center-text"><button  class="cws-button bt-color-3 border-radius alt icon-right" class="butt">View all Undergraduate Programmes <i class="fa fa-angle-right" ></i></button></h2></a>
								</div>
						   </div>
						</div>
					</div>
				</section>
			</main>
		</div>