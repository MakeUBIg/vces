<div class="page-title">
	<div class="grid-row">
		<h1>About Us</h1>
		<!-- bread crumb -->
		<nav class="bread-crumb">
			<a href="index.html">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="">About Us</a>
		</nav>
		<!-- / bread crumb -->
	</div>
</div>

	<div class="page-content">
		<main>
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-6">
							<h4>Mission Statement</h4>
							<p>VCES is set up to offer reliable and honest services supporting the prospective applicants who wish to pursue higher education or wish to immigrate to Canada.
                            VECS is formed by the Ex Georgians to support fellow Georgians / Wards who wish to apply in
                            Canadian Universities & Colleges.</p>
							<!-- accordions -->
							<h5>Core Values</h5>
							<div class="accordions">
								<!-- content-title -->
								<div class="content-title active">Professionalism</div>
								<!--/content-title -->
								<!-- accordions content -->
								<!-- <div class="content">Phasellus viverra nulla ut metus varius laoreet.</div> -->
								<!--/accordions content -->
								<!-- content-title -->
								<div class="content-title">Service and accountability</div>
								<!--/content-title -->
								<!-- accordions content -->
								<!-- <div class="content">Nullam elementum tristique risus nec pellentesque. l</div> -->
								<!--/accordions content -->
								<!-- content-title -->
								<div class="content-title">Internal and external relationships </div>
								<!--/content-title -->
								<!-- accordions content -->
								<!-- <div class="content">Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. </div> -->
								<!--/accordions content -->
								<!-- content-title -->
								<div class="content-title">Collaboration</div>
								<!--/content-title -->
								<!-- accordions content -->
								<!-- <div class="content">Phasellus viverra nulla ut metus varius laoreet.</div> -->
								<div class="content-title">Continuous improvement</div>
								<!--/content-title -->
								<!-- accordions content -->
								<!-- <div class="content">Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</div> -->
								<div class="content-title">Enthusiasm, flexibility and innovation</div>
								<!--/content-title -->
								<!-- accordions content -->
								<!-- <div class="content">Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</div> -->
								<!--/accordions content -->
							</div>
							<!--/accordions -->
<!-- 							<a href="" class="cws-button bt-color-3 border-radius alt icon-right">View Detail<i class="fa fa-angle-right"></i></a> -->
						</div>
						<div class="grid-col grid-col-6">
							<div class="owl-carousel full-width-slider">
								<div class="gallery-item picture">
									<img src="/img/People.jpg" data-at2x="/img/People.jpg" alt>
								</div>
								<div class="gallery-item picture">
									<img src="/img/Business .jpg" data-at2x="/img/Business .jpg" alt>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-12">
							<p>VCES is set up to offer reliable and honest service to the Georgian’s community and others to fill the gaps, which exist not having another choice for the community to pursue higher education or immigrate to other countries, like Canada.  Traditionally all the five “Military Schools” focus on laying down the path for the students to join the armed forces. </p>
							<p>This organization is formed by the Georgians for the Georgians. Our team includes army officers (Retd), experienced Canadian Immigration lawyers located in Canada, Placements directors who are in the business from last 18 years and individuals who are working within the Canadian Government who understands the various regulations.</p>
							<!-- accordions -->
						</div>
					</div>
				</div>
			</section>





			
	</div>