		<div class="page-title">
			<div class="grid-row">
				<h1>Undergraduate (Bachelor) Programs</h1>
				<!-- bread crumb -->
				<nav class="bread-crumb">
					<a href="index.html">Home</a>
					<i class="fa fa-long-arrow-right"></i>
					<a href="">Undergraduate (Bachelor) Programs</a>
				</nav>
				<!-- / bread crumb -->
			</div>
		</div>
		<!-- / page title -->
	<!-- / page header -->
	<!-- content -->
	<div class="page-content">
		<div class="container clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<!-- main content -->
					<main>
						<div class="comments">
							<div class="comment-title">Undergraduate (Bachelor) Programs</div>
							<ol class="commentlist">
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Applied Science (BASc, CAS)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Accounting (BComm, Business Finance, Commerce)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Biology (BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Business Administration (BBA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Chemistry (BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Computer Science (BCS, BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Economics (BA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Education (BEd)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Engineering (BASc, CAS)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Environmental Geoscience (BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Environmental Science (BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Geology (BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Kinesiology (BKin)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Mathematics and Statistics (BSc, BA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Mathematics Education (Integrated BSc+BEd)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Nutrition and Dietetics (BSN)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Philosophy (BA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Physics (BSc)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Politics (BA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Psychology (BSc, BA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Books new.jpg" data-at2x="/img/Books new.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Sociology (BA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
							</ol>
						</div>
					</main>
					<!-- / main content -->
				</div>
				<!-- sidebar -->
				<!-- / sidebar -->
			</div>
		</div>
	</div>