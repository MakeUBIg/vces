<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
        <div class="page-title">
            <div class="grid-row">
                <h1>Contact Us</h1>
                <!-- bread crumb -->
                <nav class="bread-crumb">
                    <a href="/">Home</a>
                    <i class="fa fa-long-arrow-right"></i>
                    <a href="#">Contact Us</a>
                </nav>
                <!-- / bread crumb -->
            </div>
        </div>
        <!-- page title -->
    <!-- / page header -->
    <!-- page content -->
    <div class="page-content woocommerce">
        <!-- map -->
       
        <!-- / map -->
        <!-- contact form section -->
        <div class="grid-row clear-fix" style="margin-bottom: 25px;">
            <div class="grid-col-row">
                <div class="grid-col grid-col-12">
                    <p>Having offices in India and Canada will make it easier for the clients to access the services of VCES and show our Honesty, integrity and set up an accountability towards our clients.</p>
                    <h2>Location</h2>
                    <div class="grid-col grid-col-3">
                        <p><strong class="fs-18">Canada: 009028097789</strong><br/>
                            Halifax<br/>
                            Toronto (Brampton)
                        </p>
                    </div>
                    <div class="grid-col grid-col-3">
                        <p><strong class="fs-18">India: +91 9810293046</strong><br/>
                            Bengaluru<br/>
                            New Delhi
                        </p>
                    </div>
                    <div class="grid-col grid-col-3">
                        <p><strong class="fs-18">Hours of Operation</strong><br/>
                            Canada: 9:00 am to 4:00 pm EST<br/>
                            India: 9:00 am to 5:00 pm IST
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <hr class="divider-color">
        <div class="page-content">
            <div class="container clear-fix">
                <div class="grid-col-row">
                    <div class="grid-col grid-col-6">
                        <!-- main content -->
                        <main>
                            <!-- item -->
                            <h2>Enquiry Form</h2>
                            <?php $form = ActiveForm::begin(['id' => 'contact', 'method' => 'post']); ?>

                               <?= $form->field($contact, 'name')->textInput(['maxlength' => 30,'placeholder' => 'Name'])->label(false);?>
                               <?= $form->field($contact, 'email')->textInput(['maxlength' => 30,'placeholder' => 'Email'])->label(false);?>
                                <?= $form->field($contact, 'mobile')->textInput(['maxlength' => 100,'placeholder' => 'Mobile'])->label(false);?>
                                <?= $form->field($contact, 'programm')->dropDownList([ 'undergraduate' => 'undergraduate programme', 'graduate' => 'graduate programme' ], ['prompt' => 'Select Course']);?>
                                <?= $form->field($contact, 'message')->textInput(['maxlength' => 12,'placeholder' => 'Message'])->label(false);?>

                            <?= Html::submitButton($contact->isNewRecord ? 'Submit' : 'Update', ['class' => $contact->isNewRecord ? 'cws-button bt-color-3 border-radius alt icon-right' : 'btn btn-primary']);?>

                        <br/><br/>
                        <?php ActiveForm::end(); ?>
                        </main>
                    </div>
                    <div class="grid-col grid-col-6">
                        <div class="gallery-item picture">
                            <img src="/img/driveway.jpg" data-at2x="/img/driveway.jpg" alt>
                        </div>
                    </div>
                    <!-- / side bar -->
                </div>
            </div>
        </div>
</div>

<hr class="divider-color">
        <div class="page-content">
            <div class="container clear-fix">
        
            <div class="row">
               <div class="grid-col grid-col-5">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1418.9459111487386!2d-63.50035314187006!3d44.660561794774914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b5a2364eef5947d%3A0xe87998924be7667c!2s153+Pebble+Creek+Crescent%2C+Dartmouth%2C+NS+B2W+0H6%2C+Canada!5e0!3m2!1sen!2sin!4v1518177540055" width="560" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
               </div>
               <div class="grid-col grid-col-5">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3507.003734411441!2d77.09254901467617!3d28.479434182478457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d19294876d5fb%3A0xa3a81b31fb4315e1!2s23%2C+H+36+Ln%2C+Block+H%2C+DLF+Phase+1%2C+Sector+26%2C+Gurugram%2C+Haryana+122002!5e0!3m2!1sen!2sin!4v1518430476644" width="540" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
               </div>
           </div>
       </div>
           

        <!-- / contact form section -->