<div class="page-title">
	<div class="grid-row">
		<h1>Admission Procedure</h1>
		<nav class="bread-crumb">
			<a href="index.html">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="">Admission procedure</a>
		</nav>
	</div>
</div>
<div class="page-content">
	<div class="container">
		<main>
			<h3>When to apply</h3>
			<p>You can apply anytime. However, Feb 1st is the deadline for receiving applications. Program starts in September. Applications for programs starting in winter (January) needs to be sent by May 1st.</p>
			<h3>How to apply</h3>
			<p>To study in Canada you’ll need to apply for admission before applying for a student visa from the Canadian Government.</p>
			<h3>Steps to follow:</h3>
			<div style="margin-left:30px;">
				<ol>
				<li><p>Decide on your preferred course and institution.(We will help you in selecting the program based on your education)</p></li>
				<li><p>Submit your application. (We will submit the application on your behalf)</p></li>
				<li><p>Receive your Letter of Acceptance.</p></li>
				<li><p>Apply for your student visa.(We will submit the application on your behalf)</p></li>
				<li><p>There is a range of entry requirements that you will need to meet both for university application and your visa application. This can include:</p></li>
				<li><p>Academic requirements.</p></li>
				<li><p>English language requirements.</p></li>
				<li><p>Evidence of funds to support your study.</p></li>
				<li><p>Overseas student health cover.</p></li>
				</ol>
			</div>
		</main>
	</div>
</div>