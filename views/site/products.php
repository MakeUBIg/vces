	
        <div class="page-title">
            <div class="grid-row">
                <h1>Description of Business</h1>
                <nav class="bread-crumb">
                    <a href="index.html">Home</a>
                    <i class="fa fa-long-arrow-right"></i>
                    <a href="">Description of Business</a>
                </nav>
            </div>
        </div>

	<div class="page-content">
		<main>


			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-12">
							<h4>Education:</h4>
							<p>Want to pursue education in Canada. Look no further, Valiant Canada Education Services is a consultancy service based in Halifax and Toronto, Canada with offices in India (Delhi and Bengaluru).
							</p>
							<p>We help students like you to choose a program and educational institution that not only fits your interests, but will also help you to develop a secure career plan. Our registered counsellors will support you from beginning to end, using an integrated approach that draws on their deep understanding of the process. They’ll also tap into their personal experience of moving from India to study and work in Canada and permanent settling here.
							</p>
							<!-- accordions -->
						</div>
					</div>
				</div>
			</section>


			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-6">
							<div class="main-features">
								<h4>Universities and colleges in Nova Scotia</h4>
							</div>
							<ul class="clear-fix">
								<li>Acadia University</li>
								<li>Dalhousie University</li>
								<li>Mount Sent Vincent University</li>
								<li>Nova Scotia Community College</li>
								<li>St Mary’s University</li>
								<li>St Frances Xavier University</li>
								<li>University of Kings College</li>
								<li>University of Sainte Anne</li>
								<li>Cape Breton University</li>
								<li>NSCAD University</li>
								<li>Nova Scotia Community College</li>
							</ul>
						</div>
						<div class="grid-col grid-col-6">
							<div class="main-features">
								<h4>Undergraduate (Bachelor) Programs</h4>
							</div>
							<ul class="clear-fix">
								<li>Applied Science (BASc, CAS)</li>
								<li>Accounting (BComm, Business Finance, Commerce)</li>
								<li>Biology (BSc)</li>
								<li>Business Administration (BBA)</li>
								<li>Chemistry (BSc)</li>
								<li>Computer Science (BCS, BSc)</li>
								<li>Economics (BA)</li>
								<li>Education (BEd)</li>
								<li>Engineering (BASc, CAS)</li>
								<li>Environmental Geoscience (BSc)</li>
								<li>Environmental Science (BSc)</li>
								<li>Geology (BSc)</li>
								<li>Kinesiology (BKin)</li>
								<li>Mathematics and Statistics (BSc, BA)</li>
								<li>Mathematics Education (Integrated BSc+BEd)</li>
								<li>Nutrition and Dietetics (BSN)</li>
								<li>Philosophy (BA)</li>
								<li>Physics (BSc)</li>
								<li>Politics (BA)</li>
								<li>Psychology (BSc, BA)</li>
								<li>Sociology (BA)</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-12">
							<h4>Graduate (Master) Programs</h4>
							<p>Admission to graduate programs is competitive and possession of the smallest requirements does not guarantee admission. <b>Some departments and schools have added requirements.</b> Candidates for admission to the graduate programs of most Universities must have an honor’s degree, or a four-year bachelor's degree, or its equivalent, from an approved university. Those candidates having a major in a field other than that of their graduate program will normally be required to take sufficient undergraduate courses to make up the equivalent of an undergraduate major. Special consideration may be given to those candidates wishing to change from one undergraduate field to a related graduate one.
							Candidates must have at least a B- average (70%) in the courses taken in the major field in the last two undergraduate years (or 60 credit hours) of university study, including coursework in undergraduate degree(s) and any graduate work completed.
							</p>
							<p>The number of students admitted are limited and possession of the least entrance requirements does not ensure admission. If you have any questions, please do not hesitate to contact us by emailing info@vces.ca
							</p>
							<!-- accordions -->
						</div>
					</div>
				</div>
			</section>

			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-12">
							<ul class="clear-fix">
								<li>Computer Science</li>
								<li>Business Administration (MBA)</li>
								<li>Public Administration</li>
								<li>Law</li>
								<li>Nurse Practitioner Studies</li>
								<li>Environmental Design Studies</li>
								<li>Pharmacy</li>
								<li>Dentistry</li>
								<li>Architecture</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="grid-row clear-fix">
					<div class="grid-col-row">
						<div class="grid-col grid-col-12">
							<h4>Admission Requirements</h4>
							<p><b>India</b> - Higher Secondary Certificate with min Second Class/Division ranking and minimum 50% (60% in required courses) (Undergraduate Studies)</p>
							<li>All applications must be supported by official copies of all high school transcripts and other records of academic work.</li>
							<li>Transcripts should be certified by the school and prepared in a sealed envelope by the school. They can either be sent to office directly from the school or given to the student to send. Certified transcripts acceptable will be used as a basis of admission.</li>
							<li>Admission requirements differ depending on the student's educational system. We review all applications from international students individually.</li>
							<li>Applicants for admission from another university or college must have an official transcript sent directly from that institution for evaluation.</li>
							<li>Transfer credits may be given for individual courses that are applicable to the intended undergraduate degree program of study. Course descriptions must be included with the transcripts.</li>
							<li>In order to obtain a Study Permit, application materials must be evaluated and an offer of admission must be made. When the offer of admission has been made, a letter will be issued, which is required to obtain a Study Permit.<</li>


							<h4>English requirements</h4>
							<p>Since English is the language of instruction at most Universities and colleges, candidates must be able to communicate competently in English both orally and in writing, and students whose first language is not English may be required to present the result of the TOEFL, IELTS, CAEL or PTE Academic. English requirements are as follows:
							</p>
							<li><b>TOEFL </b>- 90 IBT with no subtest score below 20</li>
							<li><b>IELTS </b>- 6.5 with no subtest score below 6.0 (General)</li>
							<li><b>CAEL </b>- 70 with no subtest score below 60</li>
							<li><b>PTE Academic </b>- 61 with no subtest score below 60</li>
							<li><b>Cambridge English:</b>  Advanced - 176 with no subtest score below 169</li>
							<li><b>MELAB </b>– 80</li>
							<!-- accordions -->
						</div>
					</div>
				</div>
			</section>

		</main>
	</div>