		<div class="page-title">
			<div class="grid-row">
				<h1>Graduate (Master) Programs</h1>
				<!-- bread crumb -->
				<nav class="bread-crumb">
					<a href="index.html">Home</a>
					<i class="fa fa-long-arrow-right"></i>
					<a href="">Graduate (Master) Programs</a>
				</nav>
				<!-- / bread crumb -->
			</div>
		</div>
		<!-- / page title -->
	<!-- / page header -->
	<!-- content -->
	<div class="page-content">
		<div class="container clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<!-- main content -->
					<main>



						<section>
							<div class="grid-row clear-fix">
								<div class="grid-col-row">
									<div class="grid-col grid-col-12">
										<h3>Graduate (Master) Programs</h3>
										<p>Admission to graduate programs is competitive and possession of the smallest requirements does not guarantee admission. Some departments and schools have added requirements.</b></br>Candidates for admission to the graduate programs of most Universities must have an honor’s degree, or a four-year bachelor's degree, or its equivalent, from an approved university. Those candidates having a major in a field other than that of their graduate program will normally be required to take sufficient undergraduate courses to make up the equivalent of an undergraduate major. Special consideration may be given to those candidates wishing to change from one undergraduate field to a related graduate one.<br>
										Candidates must have at least a B- average (70%) in the courses taken in the major field in the last two undergraduate years (or 60 credit hours) of university study, including coursework in undergraduate degree(s) and any graduate work completed.
										</p>
										<p>The number of students admitted are limited and possession of the least entrance requirements does not ensure admission.
										</p>
										<!-- accordions -->
									</div>
								</div>
							</div>
						</section>


						<div class="comments">
							<div class="comment-title"></div>
							<ol class="commentlist">
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Computer Science</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Business Administration (MBA)</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Public Administration</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Law</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Nurse Practitioner Studies</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Environmental Design Studies</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Pharmacy</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Dentistry</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="/img/Untitled cap.jpg" data-at2x="/img/Untitled cap.jpg" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Architecture</strong>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
										</div>
									</div>
								</li>
							</ol>
						</div>
					</main>
					<!-- / main content -->
				</div>
				<!-- sidebar -->
				<!-- / sidebar -->
			</div>
		</div>
	</div>