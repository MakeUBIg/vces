<!-- <style >
    input, select, textarea, button {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -webkit-appearance: button;
}input[type=checkbox],
input[type=radio] {
    visibility: block;
</style> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="grid-col-6" style="margin-left: 26em;">
<h2 style="    text-align: center;
    background: #f79a9a;">Student Registration Form</h2>

               <h4 style="background: #eadede; padding: 5px;">NAME & EMAIL</h4>
        
                    <?php $form = ActiveForm::begin(['id' => 'contact_office', 'method' => 'post']); ?>
                                <?= $form->field($registration, 'first_name')->textInput(['maxlength' => 30, 'placeholder' => 'First Name'])->label(false);?>
                                <?= $form->field($registration, 'last_name')->textInput(['maxlength' => 30,'placeholder' => 'Last Name'
                                ])->label(false);?>
                                <?= $form->field($registration, 'prefered_name')->textInput(['maxlength' => 30,'placeholder' => 'Prefered Name'])->label(false);?>
                   
               <!--  </section>
                <section class="grid-col grid-col-6 footer-latest">  --> 
                               <?= $form->field($registration, 'middle_name')->textInput(['maxlength' => 30,'placeholder' => 'Middle Name'])->label(false);?>
                               <?= $form->field($registration, 'birth_name')->textInput(['maxlength' => 30,'placeholder' => 'Birth Name'])->label(false);?>
                                <?= $form->field($registration, 'email')->textInput(['maxlength' => 100,'placeholder' => 'Email'])->label(false);?>
                <!-- </section> -->
               <br/> <h4 style="background: #eadede; padding: 5px;">PERMANENT ADDRESS</h4>
                <!--  <section class="grid-col grid-col-6 footer-about"> -->
                                <?= $form->field($registration, 'street_no')->textInput(['maxlength' => true,'placeholder' => 'Street No'])->label(false);?>
                                <?= $form->field($registration, 'address_line2')->textInput(['maxlength' => true,'placeholder' => 'Address Line2'])->label(false);?>
                                <?= $form->field($registration, 'province')->textInput(['maxlength' => 50,'placeholder' => 'Province/state'])->label(false);?>
                                <?= $form->field($registration, 'postal_zip')->textInput(['maxlength' => 12,'placeholder' => 'Postal Zip Code'])->label(false);?>
                                <?= $form->field($registration, 'work_telephone')->textInput(['maxlength' => 12,'placeholder' => 'Work Telephone'])->label(false);?>

                   
               <!--  </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                                <?= $form->field($registration, 'address_line1')->textInput(['maxlength' => true,'placeholder' => 'Address Line1'])->label(false);?>
                                <?= $form->field($registration, 'town')->textInput(['maxlength' => true,'placeholder' => 'Town/City'])->label(false);?>
                                 <?= $form->field($registration, 'country')->dropDownList([ 'canada' => 'canada', 'india' => 'india', ], ['prompt' => 'Select Country']);?>
                                 <?= $form->field($registration, 'home_phone')->textInput(['maxlength' => 12,'placeholder' => 'Home Telephone'])->label(false);?>

                <!-- </section> -->
               <br/> <h4 style="background: #eadede; padding: 5px;">PERSONAL DATA</h4>
                <!--  <section class="grid-col grid-col-6 footer-about"> -->
                               <?= $form->field($registration, 'date_of_birth')->textInput(['maxlength' => true,'placeholder' => 'Date Of Birth  /DD/MM/YYYY'])->label(false);?>
              <!--   </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                                <?= $form->field($registration, 'gender')->dropDownList([ 'male' => 'male', 'femail' => 'femail','other' => 'other' ], ['prompt' => 'Gender']);?>
                <!-- </section>
 -->               <br/> <h4 style="background: #eadede; padding: 5px;">ENROLLMENT (INTENDED MAJOR OR PROGRAM)</h4>
                <!-- <section class="grid-col grid-col-6 footer-about"> -->
                                <?= $form->field($registration, 'first_choice')->textInput(['maxlength' => true,'placeholder' => 'First Choice'])->label(false);?>
                                 <?= $form->field($registration, 'undecide')->textInput(['maxlength' => true])->label('If undecided, what subject area interestes you?');?>
               <!--  </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                                 <?= $form->field($registration, 'second_choice')->textInput(['maxlength' => true,'placeholder' => 'Second Choice'])->label(false);?>
                                 <?= $form->field($registration, 'registring_term')->textInput(['maxlength' => true,'placeholder' => 'Registring Term'])->label(false);?>
              <!--   </section> -->
               <br/> <h4 style="background: #eadede; padding: 5px;">EDUCATIONAL OBJECTIVES</h4>
               <!--  <section class="grid-col grid-col-6 footer-about"> -->
                                <?= $form->field($registration, 'immediate_education')->textInput(['maxlength' => true])->label('What is your immediate educational objective? *');?>
               <!--  </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                                <?= $form->field($registration, 'applying')->textInput(['maxlength' => true,'placeholder' => 'I am applying*'])->label(false);?>
               <!--  </section> -->
               <br/> <h4 style="background: #eadede; padding: 5px;">ENGLISH PROFICIENCY</h4>
               <!--  <section class="grid-col grid-col-6 footer-about"> -->
                                <?= $form->field($registration, 'first_launguage')->textInput(['maxlength' => true,'placeholder' => 'First Launguage'])->label(false);?>
                                <?= $form->field($registration, 'iltes_score')->textInput(['maxlength' => true,'placeholder' => 'Iltes Score'])->label(false);?>
                                <?= $form->field($registration, 'gmat_score')->textInput(['maxlength' => true,'placeholder' => 'GMAT Score'])->label(false);?>
               <!--  </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                                <?= $form->field($registration, 'launguage_thoughout')->dropDownList([ 'yes' => 'yes', 'No' => 'No', ], ['prompt' => 'Select Status']);?>
                                <?= $form->field($registration, 'tofel_score')->textInput(['maxlength' => true,'placeholder' => 'TOFEL Score'])->label(false);?>
               <!--  </section> -->
               <br/> <h4 style="background: #eadede; padding: 5px;">ACADEMIC HISTORYSECONDARY SCHOOL</h4>
                <!-- <section class="grid-col grid-col-6 footer-about"> -->
                                <?= $form->field($registration, 'official_school_name')->textInput(['maxlength' => true,'placeholder' => 'Official Name of School'])->label(false);?>                                
                                 <?= $form->field($registration, 'province_state')->textInput(['maxlength' => true,'placeholder' => 'Province / State'])->label(false);?>
               <!--  </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                              <?= $form->field($registration, 'town_city')->textInput(['maxlength' => true,'placeholder' => 'Town City'])->label(false);?>
                <!-- </section> -->
               <br/> <h4 style="background: #eadede; padding: 5px;">DATES OF ATTENDANCE</h4>
                <!-- <section class="grid-col grid-col-6 footer-about"> -->
                                <?= $form->field($registration, 'from')->textInput(['maxlength' => true,'placeholder' => 'From'])->label(false);?>
                               <?= $form->field($registration, 'year_of_graduation')->dropDownList([ '2017' => '2017', '2016' => '2016','2015' => '2015' ], ['prompt' => 'Select Status'])->label(false);?>

                <!-- </section>
                <section class="grid-col grid-col-6 footer-latest"> -->
                              <?= $form->field($registration, 'to')->textInput(['maxlength' => true,'placeholder' => 'To'])->label(false);?>
                                 
               <!--  </section>
                 <section class="grid-col grid-col-12 footer-latest"> -->
            <h6>I UNDERSTAND THAT FAILURE TO DISCLOSE MY ATTENDANCE AT ANY HIGH SCHOOL, COLLEGE OR UNIVERSITY AND FAILURE TO SUBMIT TRANSCRIPTS WHERE APPLICABLE, MAY RESULT IN THE DENIAL OF THIS APPLICATION OR SUBSEQUENT DISMISSAL FORM THE UNIVERSITY. I CERTIFY THAT TO THE BEST OF MY KNOWLEDGE ALL STATEMENTS MADE IN THIS APPLICATION ARE COMPLETE AND TRUE AND THAT ALL RECORDS ARE COMPLETE AND UNALTERED. IF ACCEPTED TO THE UNIVERSITY / COLLEGE. I AGREE TO ABIDE BY THE UNIVERSITY REGULATIONS.</h6>
       <!--  </section>
        <section class="grid-col grid-col-6 footer-about"> -->
                               <?= $form->field($registration, 'i_agree')->dropDownList([ 'yes' => 'YES, I agree to term & conditions.'],['class' => 'green'])->label(false);?><br/><br/>
               <!--  </section>
                <section class="grid-col grid-col-6 footer-about"> -->
<?= Html::submitButton($registration->isNewRecord ? 'Submit' : 'Update', ['class' => $registration->isNewRecord ? 'cws-button bt-color-3 border-radius alt icon-right' : 'btn btn-primary']);?>

                        <br/><br/>
        <?php ActiveForm::end(); ?>
           
    </div>