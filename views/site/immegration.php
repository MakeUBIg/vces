
<div class="page-title">
	<div class="grid-row">
		<h1>Immigration</h1>
		<!-- bread crumb -->
		<nav class="bread-crumb">
			<a href="index.html">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="">Immigration</a>
		</nav>
		<!-- / bread crumb -->
	</div>
</div>

<div class="page-content">
		<div class="container clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6">
					<!-- main content -->
					<div class="gallery-item picture">
						<img src="/img/passport (2).jpg" data-at2x="/img/passport.jpg" alt>
					</div>
				</div>
				<div class="grid-col grid-col-6">
					<!-- main content -->
					<main>
						<!-- item -->
						<div class="category-item list clear-fix">
							<h3>Immigration:</h3>
							<p>We just don't stop at offering you support to access the Canadian Education System, we will even go further and support you in getting the permanent immigration to Canada, as one of our Director – Jagpal Mann is an Immigration Lawyer based in Toronto, Canada with over 20 yrs of experience.
							</p>
						</div>
					</main>
				</div>
				<!-- / side bar -->
			</div>
		</div>
	</div>