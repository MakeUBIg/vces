<div class="page-title">
	<div class="grid-row">
		<h1>Admission Requirements</h1>
		<nav class="bread-crumb">
			<a href="index.html">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="">Admission Requirements</a>
		</nav>
	</div>
</div>
<div class="page-content grid-row">
	<main>
		<section class="clear-fix">
			<h2>Admission Requirements</h2>
			<p class="fs-18"><strong>India - Higher Secondary Certificate with min Second Class/Division ranking and minimum 50% (60% in required courses)</strong></p>
			<ul>
				<li>All applications must be supported by official copies of all high school transcripts and other records of academic work.</li>
				<li><p>Transcripts should be certified by the school and prepared in a sealed envelope by the school. They can either be sent to office directly from the school or given to the student to send. Certified transcripts acceptable will be used as a basis of admission.</p></li>
				<li>Admission requirements differ depending on the student's educational system. We review all applications from international students individually.</li>
				<li>Applicants for admission from another university or college must have an official transcript sent directly from that institution for evaluation.</li>
				<li>Transfer credits may be given for individual courses that are applicable to the intended undergraduate degree program of study. Course descriptions must be included with the transcripts.</li>
				<li><p>In order to obtain a Study Permit, application materials must be evaluated and an offer of admission must be made. When the offer of admission has been made, a letter will be issued, which is required to obtain a Study Permit.</p></li>
			</ul>
		</section>
			<h2>English requirements</h2>
			<p>Since English is the language of instruction at most Universities and colleges, candidates must be able to communicate competently in English both orally and in writing, and students whose first language is not English may be required to present the result of the TOEFL, IELTS, CAEL or PTE Academic. English requirements are as follows:</p>
			<ul>
				<li>TOEFL - 90 IBT with no subtest score below 20</li>
				<li>IELTS - 6.5 with no subtest score below 6.0 (General)</li>
				<li>CAEL - 70 with no subtest score below 60</li>
				<li>PTE Academic - 61 with no subtest score below 60</li>
				<li>Cambridge English:  Advanced - 176 with no subtest score below 169</li>
				<li>MELAB – 80</p></li>
			</ul>
			<p>If a student has studied in an English medium of instruction for three years, the language skill requirement may be waived.</p>
			<p>Students who do not meet our English requirements but are academically admissible may receive an offer of admission allowing them to begin academic studies upon successful completion of English for Academic Purposes (EAP) program.</p>
	</main>
</div>