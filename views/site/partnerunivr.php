	<div class="page-content woocommerce">
		<div class="container clear-fix">
			<div class="grid-col-row">

				<div class="grid-col grid-col-12">
					<br>
					<h2>Universities and colleges in Nova Scotia</h2>
					<!-- Shop -->
					<ul class="products">
						<!-- product -->
						<li class="product">
							<div class="picture">
								<img src="/img/AcadiaUniversity1.jpg" data-at2x="/img/AcadiaUniversity1.jpg" alt="">
							</div>
							<div class="product-name">
								<a href="">Acadia University</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Acadia University is a predominantly undergraduate university located in Wolfville, Nova Scotia, Canada with some graduate programs at the master's level and one at the doctoral level.</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
							<img src="/img/DalhousieUniversity.jpg" data-at2x="/img/DalhousieUniversity.jpg" alt="">
							</div>
							<div class="product-name">
								<a href="">Dalhousie University</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Dalhousie University (commonly known as Dal) is a public research university in Nova Scotia, Canada, with three campuses in Halifax, a fourth in Bible Hill, and medical teaching facilities in Saint John, New Brunswick. </p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/MountSentVinceUniversity.jpg" data-at2x="/img/MountSentVinceUniversity.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">Mount Sent Vincent University</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Mount Saint Vincent University, often referred to as The Mount, is a primarily undergraduate public university located in Halifax, Nova Scotia, Canada, and was established in 1873.</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/Nova-Scotia-Community-College.jpg" data-at2x="/img/Nova-Scotia-Community-College.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">St Frances Xavier University</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>International. St. Francis Xavier University offers a diverse, international campus within a safe and small community environment. Students from 46 countries come to Antigonish.</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/Saint.jpg" data-at2x="/img/St Mary’s University.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">St Mary’s University</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>From the moment you arrive on campus, you'll notice what makes Saint Mary's special. Our campus is compact yet cosmopolitan, making for a truly unique student experience.</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/5 nscc-strait-area-campus.jpg" data-at2x="/img/5 nscc-strait-area-campus.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">Nova Scotia Community College</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Nova Scotia Community College, commonly referred to as NSCC, is a community college serving the province of Nova Scotia through a network of 13 campuses and three community learning centres.,</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/University of Kings College.jpg" data-at2x="/img/University of Kings College.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">University of Kings College</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>King's is one of Canada's oldest and smallest universities, known for its interdisciplinary programs in the humanities and journalism. King's campus is nestled on the northwest </p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/University of Sainte Anne.jpg" data-at2x="/img/University of Sainte Anne.jpg" alt="">
								</div>
							</div>
							<div class="product-name">
								<a href="">University of Sainte Anne</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Université Sainte-Anne is a francophone university in the southwest area of Nova Scotia, Canada. It and the Université de Moncton in New Brunswick are the only French-language universities</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
						<!-- product -->
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/Cape Breton University.jpg" data-at2x="/img/Cape Breton University.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">Cape Breton University</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Cape Breton University (CBU), formerly known as the "University College of Cape Breton" (UCCB), is a Canadian university in Nova Scotia's Cape Breton Regional Municipality. Located near Sydney,</p>
								</div>
							</div>
							
						</li>
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/NSCAD University.jpg" data-at2x="/img/NSCAD University.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">NSCAD University</a>
							</div>
					Nova Scotia Community College.jpg
							<div class="product-description">
								<div class="short-description">
									<p>NSCAD University, also known as the Nova Scotia College of Art and Design, is a post-secondary art school in Halifax, Nova Scotia, Canada. It was founded in 1887 by Anna Leonowens</p>
								</div>
							</div>
							
						</li>
						<li class="product">
							<div class="picture">
								<div class="picture">
									<img src="/img/Nova Scotia Community College.jpg" data-at2x="/img/Nova Scotia Community College.jpg" alt="">
								</div>
								
							</div>
							<div class="product-name">
								<a href="">Nova Scotia Community College</a>
							</div>
					
							<div class="product-description">
								<div class="short-description">
									<p>Nova Scotia Community College, commonly referred to as NSCC, is a community college serving the province of Nova Scotia through a network of 13 campuses and three community learning centres.</p>
								</div>
							</div>
							
						</li>
						<!-- product -->
					</ul>
				</div>
			</div>
		</div>
	</div>